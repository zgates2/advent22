package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Config struct {
	path string
}

func main() {

	config := Config{
		// path: "test_input.txt",
		path: "input.txt",
	}
	filePath := config.path
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()
	var numbers []int64

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		num, err := strconv.ParseInt(strings.TrimSpace(line), 10, 64)
		if err == nil {
			numbers = append(numbers, num)
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
	}

	for outerIdx, outerNum := range numbers {
		for innerIdx, innerNum := range numbers[outerIdx+1:] {
			for _, innerInner := range numbers[innerIdx+1:] {
				if innerNum+outerNum+innerInner == 2020 {
					fmt.Println(outerNum, innerNum, innerInner)
					product := outerNum * innerNum * innerInner
					fmt.Println(product)
					return
				}
			}
		}
	}
}
