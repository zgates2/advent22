package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Config struct {
	path string
}

const ChunkCount = 3

func ParseLimits(input string) ([]string, error) {
	if input == "" {
		return nil, errors.New("Input string is empty")
	}
	split := strings.Split(input, "-")
	if len(split) != 2 {
		return nil, errors.New("Could not split input")
	}
	return split, nil
}

func main() {

	config := Config{
		//path: "test_input.txt",
		path: "input.txt",
	}
	filePath := config.path
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	var results []bool

	secondaryCount := 0
	for scanner.Scan() {
		line := scanner.Text()
		chunks := strings.Split(line, " ")
		if len(chunks) < ChunkCount {
			panic(errors.New("Invalid input data"))
		}
		limits, err := ParseLimits(chunks[0])
		if err != nil {
			panic(err)
		}

		low, err := strconv.ParseInt(limits[0], 10, 32)
		if err != nil {
			panic(err)
		}
		high, err := strconv.ParseInt(limits[1], 10, 32)
		if err != nil {
			panic(err)
		}
        count := int64(0)
		char := int(chunks[1][0])
		input := chunks[2]

		for _, c := range input {
			if int(c) == char {
				count += 1
			}
		}
		firstRegister := false
		secondRegister := false

		if len(input) > int(low-1) {
			firstRegister = int(input[low-1]) == char
		}
		if len(input) > int(high-1) {
			secondRegister = int(input[high-1]) == char
		}
		if firstRegister != secondRegister {
			secondaryCount += 1
		} 

		results = append(results, count >= low && count <= high)
	}
	count := 0
	for _, i := range results {
		if i {
			count += 1
		}
	}

	fmt.Println("count for step 1:", count)
	fmt.Println("count for step 2:", secondaryCount)
}
