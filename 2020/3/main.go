package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

// const Input = "./test_input.txt"
const Input = "./input.txt"
const SlopeX = 3
const SlopeY = 1

func RunSlope(trees []string, slopeX int, slopeY int) int {
	cursorX := 0
	cursorY := 0
	width := len(trees[0])
	height := len(trees)

	stageOneCount := 0

	for cursorY < height-slopeY {
		cursorX = (cursorX + slopeX) % width
		cursorY += slopeY

		currentChar := string(trees[cursorY][cursorX])
		if currentChar == "#" {
			stageOneCount += 1
		}
	}
	return stageOneCount
}

func main() {
	file, err := os.Open(Input)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	var trees []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		trees = append(trees, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	var counts []int
	stageOneCount := RunSlope(trees, SlopeX, SlopeY)
	fmt.Println(stageOneCount)
	for _, x := range [][]int{{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}} {
		count := RunSlope(trees, x[0], x[1])
		counts = append(counts, count)
	}
	fmt.Println(counts)

    product := 1
    for _,x := range counts {
        product *= x
    }
    fmt.Println(product)

}
