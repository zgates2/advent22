package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

// const path = "test_input.txt"
const path = "input.txt"

func withinRange(input string, min int64, max int64) (bool, error) {
	inputValue, err := strconv.ParseInt(input, 10, 64)
	if err != nil {
		return false, err
	}
	return inputValue >= min && inputValue <= max, nil
}

func byr(input string) (bool, error) {
	return withinRange(input, 1920, 2002)
}
func iyr(input string) (bool, error) {
	return withinRange(input, 2010, 2020)
}
func eyr(input string) (bool, error) {
	return withinRange(input, 2020, 2030)
}

// a number followed by either cm or in:
// If cm, the number must be at least 150 and at most 193.
// If in, the number must be at least 59 and at most 76.
func hgt(input string) (bool, error) {
	if strings.HasSuffix(input, "cm") {
		split := strings.Split(input, "cm")
		return withinRange(split[0], 150, 193)
	} else if strings.HasSuffix(input, "in") {
		split := strings.Split(input, "in")
		return withinRange(split[0], 59, 76)
	}
	return false, nil
}

// - # followed by six characters 0-9 or a-f.
func hcl(input string) (bool, error) {
	if len(input) != 7 {
		return false, nil
	}
	if string(input[0]) != "#" {
		return false, nil
	}
	allowedChars := []string{
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"a",
		"b",
		"c",
		"d",
		"e",
		"f",
	}
	for _, char := range input[1:] {
		goodchar := false
		for _, x := range allowedChars {
			if string(char) == x {
				goodchar = true
			}
		}
		if !goodchar {
			return false, nil
		}
	}
	return true, nil
}

// one of: amb blu brn gry grn hzl oth
func ecl(input string) bool {
	list := []string{
		"amb",
		"blu",
		"brn",
		"gry",
		"grn",
		"hzl",
		"oth",
	}
	for _, x := range list {
		if x == input {
			return true
		}
	}
	return false
}

// a nine-digit number, including leading zeroes.
func pid(input string) bool {
	if len(input) != 9 {
		return false
	}
	_, err := strconv.ParseInt(input, 10, 32)
	if err != nil {
		return false
	}
	return true
}

func main() {
	requiredFields := []string{
		"byr",
		"iyr",
		"eyr",
		"hgt",
		"hcl",
		"ecl",
		"pid",
	}

	file, err := os.Open(path)
	if err != nil {
		log.Fatal("Unable to open file")
	}
	defer file.Close()

	var input []string
	var buffer []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		text := scanner.Text()
		if text != "" {
			buffer = append(buffer, text)
		} else {
			input = append(input, strings.Join(buffer, " "))
			buffer = []string{}
		}
	}
	input = append(input, strings.Join(buffer, " "))
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	validDocuments := 0
	for _, document := range input {
		isValid := true
		for _, field := range requiredFields {
			if !strings.Contains(document, field) {
				isValid = false
				break
			}
		}
		if !isValid {
			continue
		} else {
			fieldsAreValid := true
			for _, field := range strings.Split(document, " ") {
				split := strings.Split(field, ":")
				switch split[0] {
				case "byr":
					val, err := byr(split[1])
					if err != nil || !val {
						fieldsAreValid = false
						break
					}
				case "iyr":
					val, err := iyr(split[1])
					if err != nil || !val {
						fieldsAreValid = false
						break
					}
				case "eyr":
					val, err := eyr(split[1])
					if err != nil || !val {
						fieldsAreValid = false
						break
					}
				case "hgt":
					val, err := hgt(split[1])
					if err != nil || !val {
						fieldsAreValid = false
						break
					}
				case "hcl":
					val, err := hcl(split[1])
					if err != nil || !val {
						fieldsAreValid = false
						break
					}
				case "ecl":
					val := ecl(split[1])
					if !val {
						fieldsAreValid = false
						break
					}
				case "pid":
					val := pid(split[1])
					if !val {
						fieldsAreValid = false
						break
					}
				}
			}
			if fieldsAreValid {
				validDocuments += 1
			}
		}
	}
	fmt.Println(validDocuments)
}
