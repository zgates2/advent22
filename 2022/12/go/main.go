package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type pointType string
type coord = struct {
	x, y int
}

const (
    InputPath string = "./full.txt"
	Start      pointType = "Start"
	Normal     pointType = "Normal"
	End        pointType = "End"
	StartChar  rune      = 'S'
	EndChar    rune      = 'E'
	LowestChar rune      = 'a'
)

type point = struct {
	pointType pointType
	steps     int
	position  coord
	height    int
}

func main() {
	file, err := os.Open(InputPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	points := [][]point{}
	cursors := []point{}
	for i, row := range rows {
		rowPoints := []point{}
		for j, char := range row {
			var newPoint point
			if char == StartChar || char == LowestChar {
				newPoint = point{pointType: Start, steps: 0, position: coord{x: j, y: i}, height: 0}
				cursors = append(cursors, newPoint)
			} else if char == EndChar {
				newPoint = point{pointType: End, steps: 0, position: coord{x: j, y: i}, height: int('z' - LowestChar)}
			} else {
				newPoint = point{pointType: Normal, steps: 0, position: coord{x: j, y: i}, height: int(char - LowestChar)}
			}
			rowPoints = append(rowPoints, newPoint)
		}
		points = append(points, rowPoints)
	}
	gameIsRunning := true
	stepCount := 0
	for gameIsRunning {
		newCursors := []point{}
		stepCount++
		for _, cursor := range cursors {
			if cursor.pointType == End {
				gameIsRunning = false
				fmt.Println("Found end. Current step count was:", stepCount-1)
				break
			}
			neighbors := getNeighbors(cursor, rows)

			neighborPoints := []point{}
			for _, neighbor := range neighbors {
				neighborPoints = append(neighborPoints, points[neighbor.y][neighbor.x])
			}
			for _, neighborPoint := range neighborPoints {
				if neighborPoint.steps == 0 && neighborPoint.height <= cursor.height+1 {
					points[neighborPoint.position.y][neighborPoint.position.x].steps = stepCount
					newCursors = append(newCursors, points[neighborPoint.position.y][neighborPoint.position.x])
				}
			}
		}
		cursors = newCursors
	}
}

func getNeighbors(p point, rows []string) []coord {
	coords := []coord{}
	width := len(rows[0]) - 1
	height := len(rows) - 1

	if p.position.y > 0 {
		coords = append(coords, coord{p.position.x, p.position.y - 1})
	}
	if p.position.y < height {
		coords = append(coords, coord{p.position.x, p.position.y + 1})
	}
	if p.position.x > 0 {
		coords = append(coords, coord{p.position.x - 1, p.position.y})
	}
	if p.position.x < width {
		coords = append(coords, coord{p.position.x + 1, p.position.y})
	}
	return coords
}
