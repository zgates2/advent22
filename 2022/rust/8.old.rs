use color_eyre::eyre::Result;
use std::fs::read_to_string;
fn main() -> Result<()> {
    // you need to count the number of trees that are visible from outside the grid
    // when looking directly along a row or column

    // A tree is visible if all of the other trees between it and an edge of the grid are shorter than it.
    // Only consider trees in the same row or column;  up, down, left, or right from any given tree.

    let input = "30373
25512
65332
33549
35390";
    let input = read_to_string("./input/8.txt")?;
    // Build Horizontal Rows
    let horizontal_rows: Vec<Vec<_>> = input
        .split("\n")
        .map(|row| {
            row.split("")
                .filter(|x| x != &"")
                .map(|x| x.parse::<u8>().unwrap())
                .collect::<Vec<u8>>()
        })
        .collect();

    let grid_size = horizontal_rows.len();

    // Build Vertical Rows
    let mut vertical_rows: Vec<Vec<u8>> = vec![];
    for y in 0..grid_size {
        let mut tmp = vec![];
        for x in 0..grid_size {
            tmp.push(horizontal_rows[x][y]);
        }
        vertical_rows.push(tmp);
    }
    // println!("vert: {:#?}", vertical_rows);

    // Build out vectors that contain the maximum visibility

    let mut lr_records: Vec<Vec<_>> = vec![];
    let mut ud_records: Vec<Vec<_>> = vec![];

    for row in horizontal_rows.iter() {
        lr_records.push(parse_vec(&row, -1)?);
    }

    for row in vertical_rows.iter() {
        ud_records.push(parse_vec(&row, -1)?);
    }

    // we'll need to transpose the values from vertical_rows onto the horizontal rows
    // to see if there are any duplicates
    for x in 0..grid_size {
        for y in 0..grid_size {
            if ud_records[x][y] == 1 {
                lr_records[y][x] = 1 as u32
            }
        }
    }
    let mut count: u32 = 0;
    for i in lr_records.clone() {
        for j in i {
            count += j as u32;
        }
    }
    dbg!(count);

    let tmp_list = lr_records
        .into_iter()
        .map(|x| {
            x.iter()
                .map(|y| y.to_string())
                .collect::<Vec<String>>()
                .join(",")
        })
        .collect::<Vec<String>>();
    for i in tmp_list {
        println!("{i}");
    }

    // Part two

    // Iterate through each tree and build
    let mut best_score = 0;
    for y in 0..grid_size {
        for x in 0..grid_size {
            // Build Left, Right, Up, Down Vecs
            let mut left: Vec<_> = vec![];
            let mut right: Vec<_> = vec![];
            for i in 0..grid_size {
                if i < x {
                    left.push(horizontal_rows[y][i])
                } else if i > x {
                    right.push(horizontal_rows[y][i])
                }
            }

            let mut up: Vec<_> = vec![];
            let mut down: Vec<_> = vec![];
            for i in 0..grid_size {
                if i < y {
                    up.push(vertical_rows[x][i])
                } else if i > y {
                    down.push(vertical_rows[x][i])
                }
            }
            // From the perspective of the tree we need to flip left and up
            left.reverse();
            up.reverse();

            for (idx, val) in [&left, &right, &up, &down].iter().enumerate() {
                // println!("{}. {:#?}", idx, val);
            }

            let tree_value = horizontal_rows[y][x];
            let l: u32 = count_trees(&left, &tree_value)?;
            let r: u32 = count_trees(&right, &tree_value)?;
            let u: u32 = count_trees(&up, &tree_value)?;
            let d: u32 = count_trees(&down, &tree_value)?;
            let score = l * r * u * d;
            if score > best_score {
                println!("x:{}, y:{}", x, y);
                println!("Set best score was:{} now:{}", best_score, score);
                best_score = score
            }
        }
    }
    dbg!(best_score);
    Ok(())
}

fn parse_vec(row: &Vec<u8>, input_threshold: i32) -> Result<Vec<u32>> {
    // println!("Parse called with {:#?}", row);
    let mut threshold: i32 = input_threshold - 1;
    let mut result = vec![];
    // forward pass
    for i in 0..row.len() {
        if row[i] as i32 > threshold {
            threshold = row[i] as i32;
            result.push(1);
        } else {
            result.push(0);
        }
    }
    threshold = input_threshold - 1;
    // dbg!(&result);
    for i in (0..row.len()).rev() {
        if row[i] as i32 > threshold {
            // println!("hit on {i}");
            threshold = row[i] as i32;
            result[i] = 1;
        }
    }
    // dbg!(&result);
    Ok(result)
}

fn count_trees(input_vec: &Vec<u8>, threshold: &u8) -> Result<u32> {
    let mut count: u32 = 0;
    for i in input_vec {
        if i < threshold {
            count += 1
        } else {
            break;
        }
    }
    Ok(count)
}

#[cfg(test)]
mod eight {
    use super::*;
    #[test]
    fn parse_vec_1() -> Result<()> {
        let mut tmp = vec![];
        for i in [3, 2, 6, 3, 3] {
            tmp.push(i as u8);
        }
        let result = parse_vec(&tmp, -1)?;
        let mut test: Vec<_> = vec![];
        for i in [1, 0, 1, 0, 1] {
            test.push(i)
        }
        assert_eq!(result, test);
        Ok(())
    }
    #[test]
    fn parse_vec_2() -> Result<()> {
        let mut tmp = vec![];
        for i in [
            3, 2, 6, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9, 9, 0,
        ] {
            tmp.push(i as u8);
        }
        let result = parse_vec(&tmp, -1)?;
        let mut test: Vec<_> = vec![];
        for i in [
            1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1,
        ] {
            test.push(i)
        }
        assert_eq!(result, test);
        Ok(())
    }
    #[test]
    fn parse_vec_empty_vector() -> Result<()> {
        let input: Vec<u8> = vec![];
        let result = parse_vec(&input, -1)?;
        dbg!(result.iter().sum::<u32>());
        assert_eq!(result.iter().sum::<u32>(), 0);
        Ok(())
    }
}
