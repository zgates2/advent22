use std::fs;
pub struct Values {
    pub elves: Vec<Vec<i32>>,
    pub sums: Vec<i32>,
    pub largest_caloric_elf: i32,
    pub sum_of_largest_three_elves: i32,
}

pub fn run() -> Values {
    let raw_data = fs::read_to_string("./input/1.txt").unwrap();
    let split_data: Vec<_> = raw_data.split("\n").collect();
    let mut elves = vec![];
    let mut tmp = vec![];
    for i in split_data {
        match i {
            "" => {
                elves.push(tmp);
                tmp = vec![];
            }
            _ => tmp.push(i.parse::<i32>().unwrap()),
        }
    }
    elves.push(tmp);
    let mut sums: Vec<i32> = vec![];
    for elf in &elves {
        sums.push(elf.iter().sum());
    }
    // Functional Map Method
    // let mut sums = elves
    //     .iter()
    //     .map(|elf| elf.iter().sum::<i32>()).collect::<Vec<_>>();
    sums.sort();

    let largest_sum = sums[sums.len() - 1];
    let three_largest_values = &sums[sums.len() - 3..];
    let sum_of_three = three_largest_values.iter().sum();

    println!("Largest sum is: {:#?}", largest_sum);
    println!("Sums of largest three values: {:#?}", sum_of_three);
    Values {
        elves: elves,
        sums: sums,
        largest_caloric_elf: largest_sum,
        sum_of_largest_three_elves: sum_of_three,
    }
}

fn main() {
    run();
}
#[cfg(test)]
mod one {
    use super::*;

    #[test]
    fn did_not_lose_any_elves() {
        let values = run();
        assert_eq!(values.elves.len(), values.sums.len());
    }
    #[test]
    fn largest_three_elves_check() {
        let values = run();
        assert_eq!(values.sum_of_largest_three_elves, 209481);
    }

    #[test]
    fn largest_elf_check() {
        let values = run();
        assert_eq!(values.largest_caloric_elf, 74711);
    }
}
