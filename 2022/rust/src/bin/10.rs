use std::fs::read_to_string;
const MAX_SCREEN_WIDTH: i32 = 40;
const FIRST_SAMPLE_IDX: i32 = 20;
const MAX_CYCLES: i32 = 221;

#[derive(Debug, PartialEq)]
enum Pixel {
    On,
    Off,
    Break,
}
struct Screen {
    rows: Vec<Pixel>,
}

impl Screen {
    fn new() -> Screen {
        Screen { rows: Vec::new() }
    }

    fn push(&mut self, pixel: Pixel) {
        let idx = self.rows.len() as i32;
        if idx % (MAX_SCREEN_WIDTH + 1) == MAX_SCREEN_WIDTH {
            self.rows.push(Pixel::Break)
        }
        self.rows.push(pixel);
    }

    fn print(&self) -> String {
        let mut output = String::new();
        for pixel in self.rows.iter() {
            match pixel {
                Pixel::On => output.push_str("#"),
                Pixel::Off => output.push_str("."),
                Pixel::Break => output.push_str("\n"),
            }
        }
        output
    }
}

#[derive(Debug, PartialEq)]
enum Command {
    Noop,
    Add,
}
impl Command {
    fn input(input: &str) -> Result<Command, &'static str> {
        match &input[..1] {
            "n" => Ok(Command::Noop),
            "a" => Ok(Command::Add),
            _ => Err("Invalid Command input"),
        }
    }
}
fn calculate_signal_strength(cycle_number: i32, register_value: i32) -> i32 {
    cycle_number * register_value
}

fn main() -> Result<(), &'static str> {
    let data = read_to_string("./input/10.txt").expect("Could not find prod file");
    // let data = read_to_string("./input/10test.txt").expect("Could not find test file");

    // Integer for current command
    let mut cycle_count = 1;
    let mut buffer: Vec<Option<i32>> = Vec::new();
    let mut register = 1;
    let mut signal_strength_history = vec![];
    let mut screen = Screen::new();

    // Fill buffer
    for line in data.split("\n") {
        let cmd = Command::input(line)?;
        match cmd {
            Command::Noop => buffer.push(None),
            Command::Add => {
                let cmd_split: Vec<&str> = line.split(" ").collect();
                let input: i32 = cmd_split[1]
                    .parse()
                    .expect("Could not convert command input into an i32");
                buffer.push(Some(input));
                buffer.push(None);
            }
        }
        // flush buffer and advance cycle
        for _ in 0..buffer.len() {
            let pixel_position = cycle_count - 1;
            // add to display row

            if (pixel_position % MAX_SCREEN_WIDTH) <= register + 1
                && (pixel_position % MAX_SCREEN_WIDTH) >= register - 1
            {
                screen.push(Pixel::On);
            } else {
                screen.push(Pixel::Off);
            }

            let action = buffer.pop().expect("Buffer processed when empty");
            match action {
                None => (),
                Some(input_value) => {
                    // Add to register
                    register += input_value
                }
            }

            if cycle_count < MAX_CYCLES
                && (cycle_count == FIRST_SAMPLE_IDX
                    || (cycle_count - FIRST_SAMPLE_IDX) % MAX_SCREEN_WIDTH == 0)
            {
                signal_strength_history.push((
                    cycle_count,
                    calculate_signal_strength(cycle_count, register),
                ))
            }
            cycle_count += 1;
        }
    }
    let total_signal_strength: i32 = signal_strength_history.iter().map(|(_, value)| value).sum();
    println!("Signal Strength: {total_signal_strength}");
    println!("{}", screen.print());
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_screen_push() {
        let mut screen = Screen::new();
        screen.push(Pixel::Break);
        screen.push(Pixel::On);
        screen.push(Pixel::Off);
        screen.push(Pixel::On);
        screen.push(Pixel::Off);

        assert_eq!(screen.rows.len(), 5);
        assert_eq!(screen.rows[0], Pixel::Break);
        assert_eq!(screen.rows[1], Pixel::On);
        assert_eq!(screen.rows[2], Pixel::Off);
        assert_eq!(screen.rows[3], Pixel::On);
        assert_eq!(screen.rows[4], Pixel::Off);
    }

    #[test]
    fn test_screen_print() {
        let mut screen = Screen::new();
        screen.push(Pixel::On);
        screen.push(Pixel::Off);
        screen.push(Pixel::Break);
        screen.push(Pixel::On);
        screen.push(Pixel::Off);
        screen.push(Pixel::Break);

        let expected_output = "#.\n#.\n";
        assert_eq!(screen.print(), expected_output);
    }

    #[test]
    fn test_command_input() {
        assert_eq!(Command::input("n").unwrap(), Command::Noop);
        assert_eq!(Command::input("a").unwrap(), Command::Add);
        assert!(Command::input("x").is_err());
    }

    #[test]
    fn test_calculate_signal_strength() {
        assert_eq!(calculate_signal_strength(2, 3), 6);
        assert_eq!(calculate_signal_strength(5, -2), -10);
    }
}
