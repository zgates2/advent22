use std::collections::VecDeque;
use std::fs::read_to_string;

/// If None Operation should be taken against its own value
#[derive(Debug)]
enum Operation {
    Multiply(Option<u128>),
    Add(Option<u128>),
}
#[derive(Debug)]
struct Target {
    if_true: usize,
    if_false: usize,
}

fn yank_trailing_int_from_str(input: &str) -> u128 {
    input
        .split(' ')
        .collect::<Vec<&str>>()
        .pop()
        .expect("Empty string was passed to function.")
        .parse()
        .expect("Was unable to parse int into a u128")
}

#[derive(Debug)]
struct Monkey {
    label: usize,
    items: VecDeque<u128>,
    target: Target,
    test: u128,
    operation: Operation,
    inspection_count: u128,
}

impl Monkey {
    fn parse(label: usize, raw_monkey: &mut Vec<&str>) -> Result<Monkey, &'static str> {
        if raw_monkey.len() != 6 {
            Err("Invalid raw_monkey length passed")
        } else {
            let raw_target_false = raw_monkey.pop().expect("Missing required field.");
            let raw_target_true = raw_monkey.pop().expect("Missing required field.");
            let target_false: usize = yank_trailing_int_from_str(raw_target_false)
                .try_into()
                .expect("Could not convert from u128 to usize");
            let target_true: usize = yank_trailing_int_from_str(raw_target_true)
                .try_into()
                .expect("Could not convert from u128 to usize");
            let target = Target {
                if_true: target_true,
                if_false: target_false,
            };

            let raw_test = raw_monkey.pop().expect("Missing required field.");
            let test = yank_trailing_int_from_str(raw_test);

            let raw_operation = raw_monkey.pop().expect("Missing required field.");
            let operation_is_add: bool = raw_operation.contains('+');

            let operation: Operation =
                if raw_operation.split("old").collect::<Vec<&str>>().len() > 2 {
                    match operation_is_add {
                        true => Operation::Add(None),
                        false => Operation::Multiply(None),
                    }
                } else {
                    let value = yank_trailing_int_from_str(raw_operation);
                    match operation_is_add {
                        true => Operation::Add(Some(value)),
                        false => Operation::Multiply(Some(value)),
                    }
                };

            let raw_starting_items = raw_monkey.pop().expect("Missing required field.");
            let trimmed_starting_items = raw_starting_items
                .replace("Starting items: ", "")
                .replace(',', "");
            let items: VecDeque<u128> = trimmed_starting_items
                .split(' ')
                .map(|x| x.parse().expect("Could not convert item into an u128"))
                .collect();
            // let title = raw_monkey.pop().expect("Required field missing");

            Ok(Monkey {
                items,
                target,
                operation,
                test,
                label,
                inspection_count: 0,
            })
        }
    }
}

fn generate_report(monkeys: &VecDeque<Monkey>) {
    let mut inspection_counts = vec![];
    for i in monkeys.iter() {
        println!("Monkey: {}, Count: {}", i.label, i.inspection_count);
        inspection_counts.push(i.inspection_count);
    }
    inspection_counts.sort();
    let value = inspection_counts.pop().unwrap() * inspection_counts.pop().unwrap();
    println!("Value: {value}");
}

fn main() -> Result<(), &'static str> {
    let raw_data = read_to_string("./input/11.txt").expect("Failed to read input file.");
    // let raw_data = read_to_string("./input/11test.txt").expect("Failed to read input file.");
    let mut monkey_split: Vec<Vec<&str>> = raw_data
        .split("\n\n")
        .map(|x| x.trim())
        .collect::<Vec<&str>>()
        .into_iter()
        .map(|x| x.split('\n').map(|x| x.trim()).collect())
        .collect();

    let mut monkeys = VecDeque::new();
    for (label, i) in monkey_split.iter_mut().enumerate() {
        let monkey = Monkey::parse(label, i)?;
        monkeys.push_back(monkey)
    }
    let divisor_product: u128 = monkeys.iter().map(|monkey| monkey.test).product();
    dbg!(divisor_product);

    // Start rounds
    for round in 0..10000 {
        if round == 1 || round == 20 || round == 1000 {
            generate_report(&monkeys)
        }
        for idx in 0..monkeys.len() {
            // 1. choose item
            for _ in 0..monkeys[idx].items.len() {
                // Increment inspection count
                monkeys[idx].inspection_count += 1;

                // 2. and make sure the monkey has an item
                let item = monkeys[idx].items.pop_front();
                if let Some(mut worry_level) = item {
                    // Increase worry level (Operation)
                    match monkeys[idx].operation {
                        Operation::Multiply(value) => {
                            if let Some(x) = value {
                                worry_level *= x;
                            } else {
                                worry_level *= worry_level;
                            }
                        }
                        Operation::Add(value) => {
                            if let Some(x) = value {
                                worry_level += x;
                            } else {
                                worry_level += worry_level;
                            }
                        }
                    }
                    // Worry level decreased

                    // worry_level = (worry_level / 3u128).floor(); // Part 1
                    worry_level %= divisor_product; // Part 2
                                                    // Check test
                    if worry_level % monkeys[idx].test == 0u128 {
                        // perform test action
                        let target = monkeys[idx].target.if_true;

                        monkeys[target].items.push_back(worry_level);
                    } else {
                        // perform test action
                        let target = monkeys[idx].target.if_false;
                        monkeys[target].items.push_back(worry_level);
                    }
                }
            }
        }
    }
    generate_report(&monkeys);

    Ok(())
}
