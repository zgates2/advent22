use std::fs::read_to_string;
const UNICODE_BASELINE: u32 = 'a' as u32;
const UNICODE_START: u32 = 'S' as u32;
const UNICODE_END: u32 = 'E' as u32;

type Coord = (i32, i32);
#[derive(Debug)]
struct RelativePoints {
    up: Option<Coord>,
    down: Option<Coord>,
    left: Option<Coord>,
    right: Option<Coord>,
}

impl RelativePoints {
    fn new(coord: Coord, grid: &Grid) -> RelativePoints {
        let length = &grid.length;
        let width = &grid.width;
        let (x, y) = coord;

        let up = if y - 1 < 0 { None } else { Some((x, y - 1)) };

        let down = if y + 1 > *length {
            None
        } else {
            Some((x, y + 1))
        };
        let left = if x - 1 < 0 { None } else { Some((x - 1, y)) };
        let right = if x + 1 > *width {
            None
        } else {
            Some((x + 1, y))
        };

        RelativePoints {
            up,
            down,
            left,
            right,
        }
    }
    fn list(&self) -> Vec<Option<Coord>> {
        vec![self.up, self.down, self.left, self.right]
    }
}

#[derive(Debug, PartialEq)]
enum PointType {
    Start,
    End,
    Normal,
}

#[derive(Debug)]
struct Point {
    kind: PointType,
    x: i32,
    y: i32,
    height: u32,
    trip_length: Option<u32>,
}
impl Point {
    fn generate_type_and_height(c: char) -> (PointType, u32) {
        let input = c as u32;
        let kind = match input {
            UNICODE_START => PointType::Start,
            UNICODE_END => PointType::End,
            _ => PointType::Normal,
        };

        let height = match kind {
            PointType::Start => 0,
            PointType::End => 'z' as u32 - UNICODE_BASELINE,
            PointType::Normal => input - UNICODE_BASELINE,
        };
        (kind, height)
    }
    fn check_update(&mut self, new_value: u32) -> Option<()> {
        dbg!(new_value);
        
        if let Some(trip_length) = self.trip_length {
            if new_value < trip_length {
                // Need to update existing value
                self.trip_length = Some(new_value);
                return Some(());
            } else {None}
        } else {
            // No value previous stated, return Some()
            self.trip_length = Some(new_value);
            Some(())
        }
    }
}

struct Grid {
    ticks: usize,
    points: Vec<Point>,
    width: i32,
    length: i32,
    starting_point: Coord,
    ending_point: Coord,
    paths: Vec<Coord>,
}

impl Grid {
    fn new(string: String) -> Grid {
        let mut points: Vec<Point> = vec![];
        let line_split: Vec<&str> = string.split("\n").collect();
        let width = line_split[0].len() as i32;
        let length = line_split.len() as i32;
        let mut starting_point = (1i32, 1i32);
        let mut ending_point = (1i32, 1i32);

        for (y, line) in line_split.into_iter().enumerate() {
            for (x, c) in line.chars().enumerate() {
                let (kind, height) = Point::generate_type_and_height(c);
                let x = x as i32;
                let y = y as i32;
                let mut trip_length = None;
                if kind == PointType::Start {
                    starting_point = (x, y);
                    trip_length = Some(0);
                } else if kind == PointType::End {
                    ending_point = (x, y)
                }
                println!("Char was {c}:{height} ({x},{y})");
                points.push(Point {
                    kind,
                    x,
                    y,
                    height,
                    trip_length,
                });
            }
        }

        Grid {
            points,
            width,
            length,
            starting_point,
            ending_point,
            paths: vec![starting_point],
            ticks: 0,
        }
    }
    fn index_from_coord(coord: &Coord, width: i32) -> usize {
        let (x, y) = coord;
        (y * width + x) as usize
    }
    fn move_possible(&self, start_coord: &Coord, end_coord: &Coord) -> bool {
        let end_index = Grid::index_from_coord(&end_coord, self.width);
        let start_index = Grid::index_from_coord(&start_coord, self.width);

        if self.points[end_index].trip_length.is_some() {
            false
        } else {
            let end_height = self.points[end_index].height;
            let start_height = self.points[start_index].height;

            start_height.abs_diff(end_height) > 1
        }
    }

    fn tick(&mut self) {
        self.ticks += 1;
        dbg!(self.ticks);
        dbg!(&self.paths);
        for _ in 0..self.paths.len() {
            let coord = self.paths.pop().unwrap();
            let relative_points = RelativePoints::new(coord, self);
            dbg!(&relative_points);

            for rp in relative_points.list() {
                println!("Hit");
                if let Some(target_coord) = rp {
                    println!("Hit some");
                    if self.move_possible(&coord, &target_coord) {
                        let target_index = Grid::index_from_coord(&target_coord, self.width);
                        let update_action =
                            self.points[target_index].check_update(self.ticks as u32);
                        match update_action {
                            Some(_) => self.paths.push(target_coord),
                            None => (),
                        }
                    }
                }
            }
        }
    }

    fn print_height_map(&self) {
        let mut row_tracker = 0;
        for point in self.points.iter() {
            if point.y > row_tracker {
                print!("\n");
                row_tracker += 1;
            }
            let height_display = if point.height > 9 {
                format!("_{}_", point.height)
            } else {
                format!("_ {}_", point.height)
            };
            print!("{}", height_display);
        }
        print!("\n");
    }
    fn print_trip_map(&self) {
        let mut row_tracker = 0;
        for point in self.points.iter() {
            if point.y > row_tracker {
                print!("\n");
                row_tracker += 1;
            }
            match point.trip_length {
                Some(trip) => {
                    if trip < 10 {
                        print!("_ {}_", trip)
                    } else {
                        print!("_{}_", trip)
                    }
                }
                None => print!("_[]_"),
            };
        }
        print!("\n");
    }
}

fn main() -> Result<(), &'static str> {
    let raw_data = read_to_string("./input/12test.txt").expect("Could not find test data.");
    let mut grid = Grid::new(raw_data);

    println!("--{}--", grid.ticks);
    grid.print_height_map();
    println!("");
    grid.print_trip_map();
    println!("");

    grid.tick();
    println!("--{}--", grid.ticks);
    grid.print_height_map();
    println!("");
    grid.print_trip_map();
    println!("");

    grid.tick();
    println!("--{}--", grid.ticks);
    grid.print_height_map();
    println!("");
    grid.print_trip_map();
    println!("");

    grid.tick();
    println!("--{}--", grid.ticks);
    grid.print_height_map();
    println!("");
    grid.print_trip_map();
    println!("");
    
    println!("{:#?}", grid.points);



    Ok(())
}
