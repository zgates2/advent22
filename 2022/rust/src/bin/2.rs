use std::fs;

fn lookup_draw(input: &str) -> &str {
    match input {
        "A" => "X",
        "B" => "Y",
        "C" => "Z",
        _ => panic!(),
    }
}
fn lookup_loss(input: &str) -> &str {
    match input {
        "A" => "Z",
        "B" => "X",
        "C" => "Y",
        _ => panic!(),
    }
}
fn lookup_win(input: &str) -> &str {
    match input {
        "A" => "Y",
        "B" => "Z",
        "C" => "X",
        _ => panic!(),
    }
}
fn shape_score(input: &str) -> i32 {
    match input {
        "X" => 1,
        "Y" => 2,
        "Z" => 3,
        _ => panic!(),
    }
}
fn score(input: &Vec<&str>) -> i32 {
    let bonus = shape_score(input[1]);
    let score = if input[1] == lookup_draw(input[0]) {
        3
    } else if input[1] == lookup_loss(input[0]) {
        0
    } else {
        6
    };
    score + bonus
}
fn choose_shape<'a>(reference: &'a str, result: &str) -> &'a str {
    match result {
        "X" => lookup_loss(reference),
        "Y" => lookup_draw(reference),
        "Z" => lookup_win(reference),
        _ => panic!(),
    }
}

fn main() {
    let raw_data = fs::read_to_string("./input.txt").unwrap();
    let data: Vec<_> = raw_data
        .split('\n')
        .map(|row| row.split(' ').collect::<Vec<&str>>())
        .collect();

    let part_one_scores: Vec<_> = data.iter().map(score).collect();
    let part_one_answer: i32 = part_one_scores.iter().sum();

    let part_two_scores: Vec<_> = data
        .iter()
        .map(|row| vec![row[0], choose_shape(row[0], row[1])])
        .map(|row| score(&row))
        .collect();

    let part_two_answer: i32 = part_two_scores.iter().sum();

    println!("{:#?}", part_one_answer);
    println!("{:#?}", part_two_answer);
}

#[cfg(test)]
mod two {
    #[test]
    fn it_works() {
        assert_eq!(1, 1);
    }
}
