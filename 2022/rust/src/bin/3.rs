use std::fs;

static ALPHA_SCORE_CARD: [char; 53] = [
    '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];

fn split_string(input: &str) -> Vec<&str> {
    let midpoint = input.len() / 2;
    vec![&input[..midpoint], &input[midpoint..]]
}
fn ident_duplicate_char(value_pair: Vec<&str>) -> char {
    for c in value_pair[0].chars() {
        if value_pair[1].contains(c) {
            return c;
        }
    }
    panic!("No duplicate value found.");
}
fn ident_triplicate_char(value_tripe: Vec<&str>) -> char {
    for c in value_tripe[0].chars() {
        if value_tripe[1].contains(c) && value_tripe[2].contains(c) {
            return c;
        }
    }
    panic!("No triplicate value found.");
}
fn lookup_char_score(c: char) -> u32 {
    ALPHA_SCORE_CARD.iter().position(|val| val == &c).unwrap() as u32
}
fn main() {
    let raw_data = fs::read_to_string("./input/3.txt").unwrap();
    let data: Vec<_> = raw_data.split("\n").collect();
    let results: Vec<_> = data
        .iter()
        .map(|x| lookup_char_score(ident_duplicate_char(split_string(x))))
        .collect();
    let total: u32 = results.iter().sum();
    println!("{}", total);

    let data_triplets: Vec<Vec<&str>> = data.chunks(3).map(|triplet| triplet.into()).collect();
    let second_part_results: Vec<_> = data_triplets
        .into_iter()
        .map(|x| lookup_char_score(ident_triplicate_char(x)))
        .collect();
    let second_part_total: u32 = second_part_results.iter().sum();
    println!("{}", second_part_total);
}

#[cfg(test)]
mod three {
    use super::*;
    #[test]
    fn split_string_functional() {
        let input = "abcd";
        let result = split_string(input);
        println!("{:#?}", result);
        assert_eq!(result[1], "cd");
    }

    #[test]
    fn duplicate_char_functional() {
        let result = ident_duplicate_char(vec!["ab", "cb"]);
        assert_eq!(result, 'b');
    }
    #[test]
    fn tripe_char_functional() {
        let result = ident_triplicate_char(vec!["ab", "ab", "zb"]);
        assert_eq!(result, 'b');
    }
    #[test]
    fn char_to_score() {
        assert_eq!(lookup_char_score('a'), 1);
        assert_eq!(lookup_char_score('b'), 2);
        assert_eq!(lookup_char_score('z'), 26);
        assert_eq!(lookup_char_score('A'), 27);
    }
}
