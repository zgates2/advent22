use std::fs;
use std::str::FromStr;

fn main() {
    // Load data
    let raw_data = fs::read_to_string("./input/4.txt").unwrap();
    let split_data: Vec<&str> = raw_data.split("\n").collect();
    let mut data = vec![];

    for row in split_data {
        let comma_split: Vec<&str> = row.split(",").collect();
        let output: Vec<Vec<u32>> = comma_split
            .into_iter()
            // Split ranges into their own vecs
            .map(|x| x.split("-").collect())
            // Convert values into u32
            .map(|x: Vec<&str>| {
                x.into_iter()
                    .map(|y| FromStr::from_str(y).unwrap())
                    .collect()
            })
            .collect();
        data.push(output);
    }

    let mut contained_count = 0;
    let mut overlap_count = 0;
    for pair in data {
        if pair[0][0] <= pair[1][0] && pair[0][1] >= pair[1][1] {
            contained_count += 1
        } else if pair[1][0] <= pair[0][0] && pair[1][1] >= pair[0][1] {
            contained_count += 1
        } else if pair[0][0] <= pair[1][0] && pair[0][1] >= pair[1][0] {
            overlap_count += 1
        } else if pair[1][0] <= pair[0][0] && pair[1][1] >= pair[0][0] {
            overlap_count += 1
        }
    }
    println!(
        "contained:{}, overlap:{}",
        contained_count,
        contained_count + overlap_count
    );
}
