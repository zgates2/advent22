use std::fs::read_to_string;
fn split_data_diagram(input: &str) -> Vec<Option<char>> {
    input
        .chars()
        .skip(1)
        .step_by(4)
        .map(|c| match c {
            '[' | ']' | ' ' => None,
            _ => Some(c),
        })
        .collect()
}

fn parse_command(input: &str) -> (usize, usize, usize) {
    let x: Vec<&str> = input.split(' ').collect();
    let output: (usize, usize, usize) = (
        x[1].parse().unwrap(),
        x[3].parse().unwrap(),
        x[5].parse().unwrap(),
    );
    output
}

fn main() {
    // Load file
    let raw_data = read_to_string("./input/5.txt").unwrap();
    let rows: Vec<_> = raw_data.split('\n').collect();
    let mut is_data_diagram = true;
    let mut raw_data_diagram = vec![];
    let mut moves = vec![];

    for row in rows {
        match row {
            "" => is_data_diagram = false,
            _ => {
                if is_data_diagram {
                    raw_data_diagram.push(row)
                } else {
                    moves.push(row)
                }
            }
        }
    }
    raw_data_diagram.pop(); // drop description line
    raw_data_diagram.reverse(); // put the top of the stack at the end of the vector

    // Generate the data_diagram
    const STACK_COUNT: usize = 9;
    let mut stacks: Vec<_> = vec![];
    for _ in 0..STACK_COUNT {
        stacks.push(vec![])
    }

    for val in raw_data_diagram {
        let sanitized = split_data_diagram(val);
        for (idx, val) in sanitized.into_iter().enumerate() {
            if val.is_some() {
                stacks[idx].push(val.unwrap())
            }
        }
    }

    let mut two_stacks = stacks.clone(); // Used for part two as we destroy the one from part 1

    // Generate the vec of commands
    let commands: Vec<(usize, usize, usize)> = moves.into_iter().map(parse_command).collect();

    for (qty, source, dest) in &commands {
        for _ in 0..*qty {
            let val = stacks[source - 1].pop();
            match val {
                None => break,
                Some(val) => stacks[dest - 1].push(val),
            }
        }
    }

    // We now need to get the top of each stack for the answer
    let answer: Vec<_> = stacks.into_iter().map(|x| x[x.len() - 1]).collect();
    println!("{:#?}", answer);

    // Part two
    for (qty, source, dest) in commands {
        let mut cache = vec![];
        for _ in 0..qty {
            let val = two_stacks[source - 1].pop();
            if val.is_none() {
                break;
            } else {
                cache.push(val.unwrap());
            }
        }
        if !cache.is_empty() {
            for _ in 0..cache.len() {
                two_stacks[dest - 1].push(cache.pop().unwrap())
            }
        }
    }
    let two_answer: Vec<_> = two_stacks.into_iter().map(|x| x[x.len() - 1]).collect();
    println!("{:#?}", two_answer);
}

#[cfg(test)]
mod five {
    use super::*;
    #[test]
    fn test_split_data_diagram() {
        let test_val = "[T] [L] [D] [G] [P] [P] [V] [N] [R]";
        let result = split_data_diagram(test_val);
        let answer = vec!['T', 'L', 'D', 'G', 'P', 'P', 'V', 'N', 'R'];
        for i in 0..answer.len() {
            assert_eq!(answer[i], result[i].unwrap());
        }
    }
    #[test]
    fn test_blanksplit() {
        let test_val = "[T]                             [R]";
        let result = split_data_diagram(test_val);
        let answer = vec![
            Some('T'),
            None,
            None,
            None,
            None,
            None,
            None,
            None,
            Some('R'),
        ];
        for i in 0..answer.len() {
            assert_eq!(answer[i], result[i]);
        }
    }
    #[test]
    fn test_parse_command() {
        let command = "move 6 from 2 to 1";
        let result = parse_command(command);
        let answer: (usize, usize, usize) = (6, 2, 1);
        assert_eq!(result, answer);
    }
    #[test]
    fn test_parse_command_large() {
        let command = "move 100 from 200 to 300";
        let result = parse_command(command);
        let answer: (usize, usize, usize) = (100, 200, 300);
        assert_eq!(result, answer);
    }
}
