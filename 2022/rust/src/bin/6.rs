use std::fs::read_to_string;
fn main() {
    let raw_data =
        read_to_string("./input/6.txt").unwrap_or_else(|err| panic!("File read failure: {}", err));
    let result = detect_start(&raw_data[..], 4)
        .unwrap_or_else(|err| panic!("Did not detect start signal: {}", err));
    println!("Part 1: {:#?}", result);
    let part_two_result =
        detect_start(&raw_data[..], 14).unwrap_or_else(|err| panic!("part two failed: {}", err));
    println!("Part 2: {:#?}", part_two_result);
}
pub trait RemoveFirst {
    type Element;
    fn remove_first(&mut self) -> Option<Self::Element>;
}

impl<T> RemoveFirst for Vec<T> {
    type Element = T;
    fn remove_first(&mut self) -> Option<T> {
        if self.is_empty() {
            return None;
        }
        Some(self.remove(0))
    }
}

fn detect_start(input: &str, size: usize) -> Result<u32, &'static str> {
    let mut count = 0;
    let mut cache = vec![];
    for c in input.chars() {
        count += 1;
        cache.push(c);
        if cache.len() > size {
            cache.remove(0);
        }
        if !vec_is_unique(&cache, size) {
            return Ok(count);
        }
    }
    Err("Failed to detect start")
}

fn vec_is_unique(input: &Vec<char>, size: usize) -> bool {
    if input.len() < size {
        return true;
    }
    let mut tmp = input.clone();
    tmp.sort();
    tmp.dedup();
    tmp.len() < size
}
#[cfg(test)]
mod six {
    use super::*;
    #[test]
    fn detect_start_test() {
        let a = detect_start("asdfasdfasdfasdfasdfasdfa", 4).unwrap();
        let b = detect_start(
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabcdzasdfasdfasdf",
            5,
        )
        .unwrap();
        assert!(a < b);
        assert_eq!(a, 4);
        assert_eq!(b, 61);
    }
    #[test]
    fn check_vec_is_unique() {
        let a = vec!['b', 'a', 'a', 'b'];
        let b = vec!['a', 'b', 'c', 'd'];
        let c = vec!['a'];
        assert!(vec_is_unique(&a, 4));
        assert!(!vec_is_unique(&b, 4));
        assert!(vec_is_unique(&c, 4));
    }
}
