use std::collections::HashMap;
use std::fs::read_to_string;

#[derive(Debug)]
enum Line {
    Cd,
    Dir,
    File,
    Ls,
}
impl Line {
    fn parse_line(input: &str) -> Line {
        match &input[..3] {
            "$ l" => Line::Ls,
            "dir" => Line::Dir,
            "$ c" => Line::Cd,
            _ => Line::File,
        }
    }
}

fn main() {
    let input = read_to_string("./input/7.txt").expect("Failed to load input file.");
    let split_input: Vec<&str> = input.split('\n').collect();
    let mut directory_sizes = HashMap::new();
    let mut pwd: Vec<String> = vec![];

    // Handle root directory
    directory_sizes.insert("root".to_string(), 0);
    pwd.push("root".to_string());

    for line in split_input {
        let command_type = Line::parse_line(line);
        println!("Line: {line} -- {command_type:#?}");
        match command_type {
            Line::Ls => {}
            Line::Cd => match line {
                "$ cd /" => {}
                "$ cd .." => {
                    pwd.pop();
                }
                x => {
                    let (_, dir_name) = x.split_once(' ').unwrap();
                    pwd.push(dir_name.to_string());
                }
            },
            Line::Dir => {
                let (_, dir_name) = line.split_once(' ').unwrap();
                directory_sizes.insert(format!("{}/{}", pwd.join("/"), dir_name), 0);
            }
            Line::File => {
                let (file_size, _) = line.split_once(' ').unwrap();

                let mut temp_path_list = pwd.clone();
                while !temp_path_list.is_empty() {
                    let temp_path = temp_path_list.join("/");
                    match directory_sizes.get(&temp_path[..]) {
                        Some(size) => {
                            directory_sizes
                                .insert(temp_path, size + file_size.parse::<i32>().unwrap());
                        }
                        _ => {}
                    }
                    temp_path_list.pop();
                }
            }
        }
    }
    let disk_usage: i32 = *directory_sizes.get("root").unwrap();
    let mut directory_size_list = vec![];
    for (_, v) in directory_sizes {
        directory_size_list.push(v);
    }
    let part_one: i32 = directory_size_list.iter().filter(|x| **x <= 100_000).sum();
    println!("{part_one}");

    let total_disk: i32 = 70_000_000;
    let disk_required = 30_000_000;
    let disk_needed = disk_required - (total_disk - disk_usage);

    directory_size_list.sort();
    for i in directory_size_list {
        if i > disk_needed {
            println!("part two: {}", i);
            break;
        }
    }
}
