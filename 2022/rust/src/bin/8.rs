use std::fs::read_to_string;

type Tree = i8;
type Row = Vec<Tree>;
struct State {
    data: Vec<DataPoint>,
}
#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Copy, Clone)]
struct DataPoint {
    value: Tree,
    visible: Option<bool>,
    visible_up: Option<u32>,
    visible_down: Option<u32>,
    visible_left: Option<u32>,
    visible_right: Option<u32>,
}
impl DataPoint {
    fn score(&self) -> u32 {
        let up = self.visible_up.unwrap_or(0);
        let down = self.visible_down.unwrap_or(0);
        let left = self.visible_left.unwrap_or(0);
        let right = self.visible_right.unwrap_or(0);
        up * down * left * right
    }
}

impl State {
    fn new() -> State {
        State { data: vec![] }
    }

    fn fetch(&self, x: u32, y: u32) -> &DataPoint {
        let idx = self.generate_index(x, y);
        &self.data[idx]
    }

    fn update(&mut self, x: u32, y: u32, visible: bool) {
        let idx = self.generate_index(x, y);
        self.data[idx].visible = Some(visible);
    }
    fn update_visible_count(&mut self, x: u32, y: u32, direction: Direction, visible_count: u32) {
        let idx = self.generate_index(x, y);
        let dp = &mut self.data[idx];
        match direction {
            Direction::Up => dp.visible_up = Some(visible_count),
            Direction::Down => dp.visible_down = Some(visible_count),
            Direction::Left => dp.visible_left = Some(visible_count),
            Direction::Right => dp.visible_right = Some(visible_count),
        };
    }

    fn generate_index(&self, x: u32, y: u32) -> usize {
        let size = f32::sqrt(self.data.len() as f32) as u32;
        let y_add = y * size;
        let index = y_add + x;
        index as usize
    }
}

fn main() {
    // let data = read_to_string("./input/8test.txt").unwrap();
    let data = read_to_string("./input/8.txt").unwrap();

    let mut state = State::new();
    let split_rows: Vec<&str> = data.split('\n').collect();
    let size = split_rows.len();
    for (y, _) in split_rows.iter().enumerate() {
        let mut row: Row = vec![];
        for c in split_rows[y].chars() {
            let tree: Tree = c.to_digit(10).unwrap().try_into().unwrap();
            row.push(tree);
            state.data.push(DataPoint {
                value: tree,
                visible: None,
                visible_down: None,
                visible_left: None,
                visible_right: None,
                visible_up: None,
            });
        }
    }

    // left to right Row
    for y in 0..size {
        let mut basis: i8 = -1;
        for x in 0..size {
            let y = y.try_into().unwrap();
            let x = x.try_into().unwrap();
            let value = state.fetch(x, y).value;
            if basis < value {
                state.update(x, y, true);
                basis = value;
            }
        }
    }

    // right to left Row
    for y in 0..size {
        let mut basis: i8 = -1;
        for x in 0..size {
            let y: u32 = (size - y - 1).try_into().unwrap();
            let x: u32 = (size - x - 1).try_into().unwrap();
            let value = state.fetch(x, y).value;
            if basis < value {
                state.update(x, y, true);
                basis = value;
            }
        }
    }

    // up to down
    for x in 0..size {
        let mut basis: i8 = -1;
        for y in 0..size {
            let y = y.try_into().unwrap();
            let x = x.try_into().unwrap();
            let value = state.fetch(x, y).value;
            if basis < value {
                state.update(x, y, true);
                basis = value;
            }
        }
    }

    // down up
    for x in 0..size {
        let mut basis: i8 = -1;
        for y in 0..size {
            let y: u32 = (size - y - 1).try_into().unwrap();
            let x: u32 = (size - x - 1).try_into().unwrap();
            let value = state.fetch(x, y).value;
            if basis < value {
                state.update(x, y, true);
                basis = value;
            }
        }
    }
    for y in 0..size {
        let y = y.try_into().unwrap();
        for x in 0..size {
            let x = x.try_into().unwrap();
            let value = state.fetch(x, y).value;
            // Visible Trees to the right
            let mut right_count = 0;
            for tree_right in (x + 1)..size as u32 {
                right_count += 1;
                let comparison_value = state.fetch(tree_right, y).value;
                if comparison_value >= value {
                    break;
                }
            }
            state.update_visible_count(x, y, Direction::Right, right_count);

            // Visible Trees to the left
            let mut left_count = 0;
            for idx in 0..x {
                left_count += 1;
                let tree_left = x - 1 - idx;
                let comparison_value = state.fetch(tree_left, y).value;
                if comparison_value >= value {
                    break;
                }
            }
            state.update_visible_count(x, y, Direction::Left, left_count);

            // Visible Trees below
            let mut down_count = 0;
            for tree_down in (y + 1)..size as u32 {
                down_count += 1;
                let comparison_value = state.fetch(x, tree_down).value;
                if comparison_value >= value {
                    break;
                }
            }
            state.update_visible_count(x, y, Direction::Down, down_count);

            // Visible Trees above
            let mut up_count = 0;
            for idx in 0..y {
                up_count += 1;
                let tree_up = y - 1 - idx;
                let comparison_value = state.fetch(x, tree_up).value;
                if comparison_value >= value {
                    break;
                }
            }
            state.update_visible_count(x, y, Direction::Up, up_count);
        }
    }
    // Now we're going to compute the scenic score for each
    let best_score = state.data.iter().map(|i| i.score()).max().unwrap_or(0);
    println!("part 2 answer: {best_score}")
}
