use core::fmt;
use std::fs::read_to_string;
use std::ops::{Add, Sub};

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn new(input: &str) -> Direction {
        match input {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            _ => panic!("Unhandled input direction"),
        }
    }
    fn translate(&self) -> Coord {
        match self {
            Direction::Up => Coord::new(0, 1),
            Direction::Down => Coord::new(0, -1),
            Direction::Left => Coord::new(-1, 0),
            Direction::Right => Coord::new(1, 0),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Coord {
    x: i32,
    y: i32,
}
impl Coord {
    fn new(x: i32, y: i32) -> Coord {
        Coord { x, y }
    }
    fn move_needed(head: &Coord, tail: &Coord) -> Option<Coord> {
        let difference = head - tail;
        if difference.x.abs() < 2 && difference.y.abs() < 2 {
            return None;
        } else {
            let left = if difference.x < 0 {
                Some(Direction::Left.translate())
            } else {
                None
            };
            let right = if difference.x > 0 {
                Some(Direction::Right.translate())
            } else {
                None
            };
            let down = if difference.y > 0 {
                Some(Direction::Up.translate())
            } else {
                None
            };
            let up = if difference.y < 0 {
                Some(Direction::Down.translate())
            } else {
                None
            };
            let result = left.unwrap_or(Coord::new(0, 0))
                + right.unwrap_or(Coord::new(0, 0))
                + up.unwrap_or(Coord::new(0, 0))
                + down.unwrap_or(Coord::new(0, 0));
            Some(result)
        }
    }
}
impl Add<Coord> for Coord {
    type Output = Coord;
    fn add(self, other: Coord) -> Coord {
        Coord {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Add<Coord> for &Coord {
    type Output = Coord;
    fn add(self, other: Coord) -> Coord {
        Coord {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}
impl Sub<&Coord> for &Coord {
    type Output = Coord;
    fn sub(self, other: &Coord) -> Coord {
        Coord {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}
impl fmt::Display for Coord {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(&self.x.to_string()[..])?;
        fmt.write_str("_")?;
        fmt.write_str(&self.y.to_string()[..])?;
        Ok(())
    }
}

enum RopeType {
    Head,
    Tail,
    Normal,
}
struct Rope {
    rope_type: RopeType,
    position: Coord,
}
impl Rope {
    fn new(rope_type: RopeType, base_position: &Coord) -> Rope {
        Rope {
            rope_type,
            position: base_position.clone(),
        }
    }
}

struct Game {
    ropes: Vec<Rope>,
    tail_history: Vec<Coord>,
}

impl Game {
    fn new(qty_of_rope: u32) -> Game {
        let base_position = Coord::new(100, 100);
        let mut ropes = vec![];
        // Add custom head
        ropes.push(Rope::new(RopeType::Head, &base_position));
        for _ in 2..qty_of_rope {
            ropes.push(Rope::new(RopeType::Normal, &base_position));
        }
        ropes.push(Rope::new(RopeType::Tail, &base_position));

        Game {
            ropes,
            tail_history: vec![base_position],
        }
    }
    fn instruction(&mut self, head_direction: &Direction) -> () {
        for idx in 0..self.ropes.len() {
            match self.ropes[idx].rope_type {
                RopeType::Head => {
                    let update_coord = head_direction.translate();
                    self.ropes[idx].position = &self.ropes[idx].position + update_coord;
                }
                RopeType::Normal => {
                    let move_needed = Coord::move_needed(
                        &self.ropes[idx - 1].position,
                        &self.ropes[idx].position,
                    );
                    if let Some(x) = move_needed {
                        self.ropes[idx].position = &self.ropes[idx].position + x;
                    }
                }
                RopeType::Tail => {
                    let move_needed = Coord::move_needed(
                        &self.ropes[idx - 1].position,
                        &self.ropes[idx].position,
                    );
                    if let Some(x) = move_needed {
                        self.ropes[idx].position = &self.ropes[idx].position + x;
                    }
                    self.tail_history.push(self.ropes[idx].position.clone());
                }
            }
        }
    }
    fn count_unique_tail_history(&self) -> u32 {
        let mut work_space = vec![];
        for coord in &self.tail_history {
            work_space.push(format!("{coord}"));
        }
        work_space.sort();
        work_space.dedup();
        work_space.len() as u32
    }
}

fn main() {
    // let data = read_to_string("./input/9test.txt").expect("Could not find test file");
    let data = read_to_string("./input/9.txt").expect("Could not find production file");
    let split_data = data.split("\n");

    let mut game = Game::new(10);
    for row in split_data {
        let split_row: Vec<&str> = row.split(" ").collect();
        if split_row.len() != 2 {
            panic!("Row contained invalid number of args")
        }
        let direction = Direction::new(split_row[0]);
        let number_of_moves: u32 = split_row[1]
            .parse::<u32>()
            .expect("Could not convert number of moves into int");
        for _ in 0..number_of_moves {
            game.instruction(&direction)
        }
    }

    println!("{}", game.count_unique_tail_history());
}
