package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	// file, err := os.Open("./1.input.txt")
	file, err := os.Open("./1.input2.txt")
	// file, err := os.Open("./1.full.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	var numberWords = [10]string{
		"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}
	sum := 0

	for scanner.Scan() {
		line := scanner.Text()

		updatedLine := ""
		for i := 0; i < len(line); i++ {
			needsUpdate := true
			for idx, num := range numberWords {
				size := len(num)
				if size+i <= len(line) {
					substr := line[i : i+size]
					if substr == num {
						updatedLine += fmt.Sprint(idx)
						needsUpdate = false
						i += size
						break
					}
				}
			}
			if needsUpdate {
				updatedLine += string(line[i])
			}
		}

		buffer := []rune{}
		for _, char := range updatedLine {
			if '0' <= char && char <= '9' {
				buffer = append(buffer, char)
			}
		}
		fmt.Println("Line:", line, "->", updatedLine)
		if len(buffer) == 0 {
			continue
		} else {
			numberString := string(buffer[0]) + string(buffer[len(buffer)-1])
			value, err := strconv.Atoi(numberString)
			if err != nil {
				log.Fatal(err)
			}
			sum += value
		}

	}
	fmt.Println("Sum:", sum)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
