import fs from "fs"

type MyTuple = [string, string, string, string, string, string, string, string, string, string]

const filepath = "1.full.txt"
if (!fs.existsSync(filepath)) throw new Error("Input file not found.")
const numberWords: MyTuple = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
const numbers: MyTuple = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
const buffer: [string, string] = ["", ""]

const data = fs.readFileSync(filepath, "utf8")
function part_one() {
    let sum = 0
    for (const line of data.split("\n")) {
        const buffer: string[] = []
        for (let i = 0; i < line.length; i++) {
            const char = line[i]
            if (numbers.includes(char)) {
                buffer[0] = char
                break
            }
        }
        for (let i = line.length; i >= 0; i--) {
            const char = line[i]
            if (numbers.includes(char)) {
                buffer[1] = char
                break
            }
        }

        if (buffer.length) {
            sum += Number(buffer[0] + buffer[1])
        }
    }
    console.log(sum)
}

function part_two() {
    let sum = 0
    for (const line of data.split("\n")) {
        for (let i = 0; i < line.length; i++) {
            const char = line[i]
            if (numbers.includes(char)) buffer.push(char)
            else {
                for (let j = 0; j < numberWords.length; j++) {
                    const numberWord = numberWords[j]
                    if (i + numberWord.length - 1 < line.length) {
                        const peak = line.substring(i, i + numberWord.length)
                        if (peak == numberWord) {
                            buffer.push(j.toString())
                            i += numberWord.length - 2
                            break
                        }
                    }
                }
            }
        }
        if (buffer.length) {
            const output = Number(buffer[0] + buffer[buffer.length - 1])
            sum += output
        }
    }
    console.log(sum)
}

console.time("one")
part_one()
console.timeEnd("one")
console.time("two")
part_two()
console.timeEnd("two")
