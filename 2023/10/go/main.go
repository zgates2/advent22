package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const (
	//	Path string = "./input.txt"
	// Path string = "./input2.txt"
	// Path string = "./input3.txt"
	Path string = "./full.txt"
)

const (
	NSPipe = byte('|')
	EWPipe = byte('-')
	NEBend = byte('L')
	NWBend = byte('J')
	SWBend = byte('7')
	SEBend = byte('F')
	Start  = byte('S')
)

// simplify PipeMap
type PipeMap struct{ up, right, down, left []byte }

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println(Part_one(rows))
	fmt.Println(Part_two(rows))
}

// Point is a point in 2D space
type Point struct {
	x, y  int
	value byte
}

func Part_one(rows []string) int {
	var start Point
	found := false
	for y := 0; y < len(rows); y++ {
		for x := 0; x < len(rows[y]); x++ {
			if rows[y][x] == Start {
				start = Point{x: x, y: y, value: rows[y][x]}
				break
			}
		}
		if found {
			break
		}
	}

	previousPoint := start
	point, err := scanForOpenPipe(start, Point{x: -1, y: -1, value: '-'}, rows)

	if err != nil {
		panic(err)
	}

	loop := true

	for i := 2; loop; i++ {
		tmp := point
		point, err = scanForOpenPipe(point, previousPoint, rows)
		previousPoint = tmp

		if comparePoints(point, start) {
			// Found the start point
			return i / 2
		}
	}
	panic("No answer found")
}

func Part_two(rows []string) int {
	// Find the starting point
	var start Point
	found := false
	for y := 0; y < len(rows); y++ {
		for x := 0; x < len(rows[y]); x++ {
			if rows[y][x] == Start {
				start = Point{x, y, rows[y][x]}
				found = true
				break
			}
		}
		if found {
			break
		}
	}

	previousPoint := start
	point, err := scanForOpenPipe(start, Point{x: -1, y: -1}, rows)
	if err != nil {
		panic(err)
	}
	polygon := []Point{previousPoint, point}

	loop := true
	for i := 2; loop; i++ {
		tmp := point
		point, err = scanForOpenPipe(point, previousPoint, rows)
		if err != nil {
			panic(err)
		}
		previousPoint = tmp
		polygon = append(polygon, point)
		if comparePoints(point, start) {
			break
		}
	}

	insideDotCount := 0
	polySimplified, err := polyString(polygon, len(rows), len(rows[0]))
	if err != nil {
		panic(err)
	}
	dots := findDots(polySimplified)

	// Full printout
	// for y := 0; y < len(rows); y++ {
	// 	for x := 0; x < len(rows[y]); x++ {
	// 		found := false
	// 		for _, dot := range dots {
	// 			if dot.x == x && dot.y == y {
	// 				if isDotInsidePolygon(dot, polygon, len(rows), len(rows[0])) {
	// 					insideDotCount++
	// 					fmt.Printf("@")
	// 					found = true
	// 				} else {
	// 					fmt.Printf("O")
	// 					found = true
	// 				}
	// 			}
	// 		}
	// 		if !found {
	// 			fmt.Printf("%s", string(rows[y][x]))
	// 		}
	// 	}
	// 	fmt.Printf("\n")
	// }
	for _, dot := range dots {
		result, err := isDotInsidePolygon(dot, polygon, len(rows), len(rows[0]))
		if err != nil {
			panic(err)
		}
		if result {
			insideDotCount++
		}
	}
	return insideDotCount
}

func isDotInsidePolygon(dot Point, poly []Point, length int, width int) (bool, error) {
	if length <= 0 || width <= 0 {
		return false, fmt.Errorf("Invalid length or width")
	}

	if dot.x == 0 || dot.y == 0 {
		return false, nil
	}
	if dot.x == width-1 || dot.y == length-1 {
		return false, nil
	}
	// Raycast Algo
	intersects := false

	for i := 0; i < len(poly); i++ {
		curr := poly[i]
		next := poly[(i+1)%len(poly)]
		if ((curr.y <= dot.y && dot.y < next.y) || (next.y <= dot.y && dot.y < curr.y)) &&
			(dot.x < (next.x-curr.x)*(dot.y-curr.y)/(next.y-curr.y)+curr.x) {
			intersects = !intersects
		}
	}
	return intersects, nil
}

func findDots(rows []string) []Point {
	dots := []Point{}
	for y := 0; y < len(rows); y++ {
		for x := 0; x < len(rows[y]); x++ {
			if rows[y][x] == '.' {
				dots = append(dots, Point{x, y, rows[y][x]})
			}
		}
	}
	return dots
}

func scanForOpenPipe(p Point, prevPoint Point, rows []string) (Point, error) {
	directionsToCheck, err := GetCompatiblePipes(rows[p.y][p.x])
	if err != nil {
		return Point{}, err
	}
	checkUp := p.y != 0 && len(directionsToCheck.up) > 0
	checkRight := p.x <= len(rows[p.y])-1 && len(directionsToCheck.right) > 0
	checkDown := p.y <= len(rows)-1 && len(directionsToCheck.down) > 0
	checkLeft := p.x != 0 && len(directionsToCheck.left) > 0

	if checkUp {
		targetDirection := rows[p.y-1][p.x]
		newPoint := Point{y: p.y - 1, x: p.x, value: targetDirection}
		for _, direction := range directionsToCheck.up {
			if targetDirection == direction &&
				!comparePoints(newPoint, prevPoint) {
				return newPoint, nil
			}
		}
	}
	if checkRight {
		targetDirection := rows[p.y][p.x+1]
		newPoint := Point{y: p.y, x: p.x + 1, value: targetDirection}
		for _, direction := range directionsToCheck.right {
			if targetDirection == direction &&
				!comparePoints(newPoint, prevPoint) {
				return newPoint, nil
			}

		}
	}
	if checkDown {
		targetDirection := rows[p.y+1][p.x]
		newPoint := Point{y: p.y + 1, x: p.x, value: targetDirection}
		for _, direction := range directionsToCheck.down {
			if targetDirection == direction &&
				!comparePoints(newPoint, prevPoint) {
				return newPoint, nil
			}
		}
	}
	if checkLeft {
		targetDirection := rows[p.y][p.x-1]
		newPoint := Point{y: p.y, x: p.x - 1, value: targetDirection}
		for _, direction := range directionsToCheck.left {
			if targetDirection == direction &&
				!comparePoints(newPoint, prevPoint) {
				return newPoint, nil
			}
		}
	}
	return Point{}, fmt.Errorf("Impossible state, no open pipe found")
}

func isInsidePolygon(p Point, poly []Point) (bool, error) {
	if poly == nil || len(poly) == 0 {
		return false, fmt.Errorf("Invalid polygon")
	}
	intersects := 0
	for i := p.y; i > 0; i-- {
		for _, polyPoint := range poly {
			if polyPoint.y == i && polyPoint.x == p.x {
				intersects++
				break
			}
		}
	}
	return intersects%2 == 1, nil
}

func GetCompatiblePipes(b byte) (PipeMap, error) {
	up := []byte{NSPipe, SWBend, SEBend, Start}
	right := []byte{EWPipe, NWBend, SWBend, Start}
	down := []byte{NSPipe, NEBend, NWBend, Start}
	left := []byte{EWPipe, NEBend, SEBend, Start}
	empty := []byte{}

	switch b {
	case '|':
		return PipeMap{
			up:    up,
			down:  down,
			left:  empty,
			right: empty,
		}, nil
	case '-':
		return PipeMap{
			left:  left,
			right: right,
			up:    empty,
			down:  empty,
		}, nil
	case 'L':
		return PipeMap{
			up:    up,
			right: right,
			down:  empty,
			left:  empty,
		}, nil
	case 'J':
		return PipeMap{
			up:    up,
			left:  left,
			down:  empty,
			right: empty,
		}, nil
	case '7':
		return PipeMap{
			left:  left,
			down:  down,
			right: empty,
			up:    empty,
		}, nil
	case 'F':
		return PipeMap{
			right: right,
			down:  down,
			up:    empty,
			left:  empty,
		}, nil
	case 'S':
		return PipeMap{
			right: right,
			down:  down,
			up:    up,
			left:  left,
		}, nil

	}
	return PipeMap{}, fmt.Errorf("No compatible pipes found for %s", string(b))
}
func polyString(poly []Point, length int, width int) ([]string, error) {
	if length <= 0 || width <= 0 {
		return []string{}, fmt.Errorf("Invalid length or width")
	}
	output := []string{}
	for y := 0; y < length; y++ {
		row := ""
		for x := 0; x < width; x++ {
			found := false
			for _, point := range poly {
				if point.x == x && point.y == y {
					found = true
					row += string(point.value)
					break
				}
			}
			if !found {
				row += "."
			}
		}
		output = append(output, row)
	}
	return output, nil
}

// Compare two points and return true if they are the same
func comparePoints(a Point, b Point) bool {
	if a.x == b.x && a.y == b.y {
		return true
	}
	return false
}
