package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const (
	// Path string = "./input.txt"
	// Path string = "./test.txt"
	Path           string = "./full.txt"
	EmptySpaceSize        = 1_000_000
	EmptyCellValue        = '.'
)

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println(Part_one(rows))
	fmt.Println(Part_two(rows))
}

func Part_two(rows []string) int {
	return calculateDistances(rows, EmptySpaceSize)
}

func calculateDistances(rows []string, spaceSize int) int {
	emptyYIndices := []int{}
	for y := 0; y < len(rows); y++ {
		foundNonDefaultValue := false
		for x := 0; x < len(rows[y]); x++ {
			if rows[y][x] != EmptyCellValue {
				foundNonDefaultValue = true
				break
			}
		}
		if !foundNonDefaultValue {
			emptyYIndices = append(emptyYIndices, y)
		}
	}
	emptyXIndices := []int{}
	for x := 0; x < len(rows[0]); x++ {
		foundNonDefaultValue := false
		for y := 0; y < len(rows); y++ {
			if rows[y][x] != EmptyCellValue {
				foundNonDefaultValue = true
				break
			}
		}
		if !foundNonDefaultValue {
			emptyXIndices = append(emptyXIndices, x)
		}
	}

	galaxies := []Point{}
	for y := 0; y < len(rows); y++ {
		for x := 0; x < len(rows[y]); x++ {
			if rows[y][x] != '.' {
				galaxies = append(galaxies, Point{x, y})
			}
		}
	}

	sum := 0
	for i := 0; i < len(galaxies); i++ {
		for j := i + 1; j < len(galaxies); j++ {
			a := galaxies[i]
			b := galaxies[j]
			if a.x == b.x && a.y == b.y {
				continue
			}
			xDistance := abs(a.x - b.x)

			for _, xSpace := range emptyXIndices {
				if xSpace > a.x && xSpace < b.x || xSpace < a.x && xSpace > b.x {
					xDistance += spaceSize - 1
				}
			}
			yDistance := abs(a.y - b.y)

			for _, ySpace := range emptyYIndices {
				if ySpace > a.y && ySpace < b.y || ySpace < a.y && ySpace > b.y {
					yDistance += spaceSize - 1
				}
			}
			sum += xDistance + yDistance
		}
	}
	return sum
}
func Part_one(rows []string) int {
	return calculateDistances(rows, 2)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

type Point struct {
	x, y int
}

func (g Point) print() {
	println(fmt.Sprintf("Point at (%d, %d)", g.x, g.y))
}
