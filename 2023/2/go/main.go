package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	Red   = "red"
	Green = "green"
	Blue  = "blue"
)

func main() {
	path := "./1.full.txt"
	part_one(path)
	part_two(path)
}

func part_one(path string) {
	cubes := map[string]int{
		Red:   12,
		Blue:  14,
		Green: 13,
	}
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	gameNumber := 0
	var gamesPossible []int
	for scanner.Scan() {
		gamePossible := true
		gameNumber += 1
		line := scanner.Text()
		gameString := strings.Split(line, ": ")[1]
		gameString = strings.ReplaceAll(gameString, ",", "")
		rounds := strings.Split(gameString, "; ")

		for _, round := range rounds {
			groups := strings.Split(round, " ")
			for i := 0; i < len(groups); i += 2 {
				count, err := strconv.Atoi(groups[i])
				if err != nil {
					log.Fatalf("Non parsable int: %v", err)
				}
				group := groups[i+1]
				if count > cubes[group] {
					gamePossible = false
					break // Group
				}
			}
			if !gamePossible {
				break // Round
			}
		}
		if gamePossible {
			gamesPossible = append(gamesPossible, gameNumber)
		}
	}
	sum := 0
	for _, x := range gamesPossible {
		sum += x
	}
	fmt.Println(sum)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func part_two(path string) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	var powers []int
	for scanner.Scan() {
		line := scanner.Text()
		gameString := strings.Split(line, ": ")[1]
		gameString = strings.ReplaceAll(gameString, ",", "")
		rounds := strings.Split(gameString, "; ")
		var r, g, b int = 1, 1, 1

		for _, round := range rounds {
			groups := strings.Split(round, " ")
			for i := 0; i < len(groups); i += 2 {
				group := groups[i+1]
				count, err := strconv.Atoi(groups[i])
				if err != nil {
					log.Fatal("Non parsable int")
				}
				switch group {
				case Red:
					if count > r {
						r = count
					}
				case Green:
					if count > g {
						g = count
					}
				case Blue:
					if count > b {
						b = count
					}
				}
			}
		}
		powers = append(powers, r*g*b)
	}
	var sum int = 0
	for _, x := range powers {
		sum += x
	}
	fmt.Println(sum)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
