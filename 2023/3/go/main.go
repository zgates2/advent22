package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	path := "./full.txt"
	fmt.Println(part_one(path))
	fmt.Println(part_two(path))
}

type point struct {
	x int
	y int
}

type symbol struct {
	position point
	text     string
	values   []int
}

func isNumberRune(char rune) bool {
	return char >= '0' && char <= '9'
}

func part_one(path string) int {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}

	symbols := []symbol{}
	for i := 0; i < len(rows); i++ {
		for j := 0; j < len(rows[i]); j++ {
			char := rune(rows[i][j])
			if !isNumberRune(char) && char != '.' {
				newSymbol := symbol{
					position: point{x: j, y: i},
					text:     string(char),
				}
				symbols = append(symbols, newSymbol)
			}
		}
	}

	sum := 0
	for i := 0; i < len(rows); i++ {
		for j := 0; j < len(rows[i]); j++ {
			if !isNumberRune(rune(rows[i][j])) {
				continue
			}
			number := string(rows[i][j])
			peakIdx := j + 1
			for k := j; j < len(rows[i]); k++ {
				if peakIdx >= len(rows[0]) || !isNumberRune(rune(rows[i][peakIdx])) {
					break
				}
				number += string(rows[i][peakIdx])
				peakIdx += 1
			}

			symbolFound := false
			start := point{x: j, y: i}
			end := point{x: peakIdx - 1, y: i}
			for _, symbol := range symbols {
				if symbol.position.x < start.x-1 {
					continue
				}
				if symbol.position.x > end.x+1 {
					continue
				}
				if symbol.position.y < start.y-1 {
					continue
				}
				if symbol.position.y > end.y+1 {
					continue
				}
				symbolFound = true
				break
			}
			if symbolFound {
				val, err := strconv.Atoi(number)
				if err != nil {
					log.Fatal(err)
				}
				sum += val
			}
			j = peakIdx
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return sum
}

func part_two(path string) int {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}

	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}

	symbols := []symbol{}
	for i := 0; i < len(rows); i++ {
		for j := 0; j < len(rows[i]); j++ {
			char := rune(rows[i][j])
			if char == '*' {
				newSymbol := symbol{
					position: point{x: j, y: i},
					text:     string(char),
				}
				symbols = append(symbols, newSymbol)
			}
		}
	}
	for i := 0; i < len(rows); i++ {
		for j := 0; j < len(rows[i]); j++ {
			if !isNumberRune(rune(rows[i][j])) {
				continue
			}
			number := string(rows[i][j])
			peakIdx := j + 1
			for k := j; j < len(rows[i]); k++ {
				if peakIdx >= len(rows[0]) || !isNumberRune(rune(rows[i][peakIdx])) {
					break
				}
				number += string(rows[i][peakIdx])
				peakIdx += 1
			}

			start := point{x: j, y: i}
			end := point{x: peakIdx - 1, y: i}
			for k, symbol := range symbols {
				if symbol.position.x < start.x-1 {
					continue
				}
				if symbol.position.x > end.x+1 {
					continue
				}
				if symbol.position.y < start.y-1 {
					continue
				}
				if symbol.position.y > end.y+1 {
					continue
				}
				numberInt, err := strconv.Atoi(number)
				if err != nil {
					log.Fatal(err)
				}
				symbols[k].values = append(symbol.values, numberInt)
			}
			j = peakIdx
		}

	}
	sum := 0
	for _, symbol := range symbols {
		if len(symbol.values) == 2 {
			sum += symbol.values[0] * symbol.values[1]
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return sum
}
