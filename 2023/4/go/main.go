package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	Input string = "./full.txt"
)

func main() {
	file, err := os.Open(Input)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}

	winPointSequence, err := generateSequence(20)
	if err != nil {
		log.Fatal(err)
	}

	part_one(rows, winPointSequence)
	part_two(rows)
}

func part_one(rows []string, winPointSequence []int) {
	totalPoints := 0
	for _, row := range rows {
		trimmedRow := strings.Split(row, ": ")[1]
		splitRow := strings.Split(trimmedRow, " | ")

		winCount := 0
		for _, winValue := range strings.Split(splitRow[0], " ") {
			for _, inputValue := range strings.Split(splitRow[1], " ") {
				if winValue == inputValue && winValue != "" {
					winCount++
					break
				}
			}
		}

		totalPoints += winPointSequence[winCount]
	}

	fmt.Println("Total Points:", totalPoints)
}

func part_two(rows []string) {

	cardCounts := make([]int, len(rows))
	for i := range cardCounts {
		cardCounts[i] = 1
	}

	for i, row := range rows {
		trimmedRow := strings.Split(row, ": ")[1]
		splitRow := strings.Split(trimmedRow, " | ")

		winCount := 0
		for _, winValue := range strings.Split(splitRow[0], " ") {
			for _, inputValue := range strings.Split(splitRow[1], " ") {
				if winValue == inputValue && winValue != "" {
					winCount++
					break
				}
			}
		}
		for k := 1; k <= winCount; k++ {
			cardCounts[i+k] += cardCounts[i]
		}
	}
	totalCards := 0
	for _, val := range cardCounts {
		totalCards += val
	}

	fmt.Println("Total Cards:", totalCards)
}

func generateSequence(max int) ([]int, error) {
	if max < 0 {
		return nil, errors.New("Negative value passed to generator")
	}
	result := []int{0, 1}
	for i := 0; i < max; i++ {
		result = append(result, result[len(result)-1]*2)
	}
	return result, nil
}
