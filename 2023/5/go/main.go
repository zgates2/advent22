package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	//"fmt"
)

const (
	// Path string = "./input.txt"
	Path string = "./full.txt"
)

type Input = struct {
	start uint64
	end   uint64
}
type Rule = struct {
	sourceStart uint64
	sourceEnd   uint64
	delta       uint64
}

type Data = struct {
	source      uint64
	destination uint64
	size        uint64
}

type Entry = struct {
	sourceName      string
	destinationName string
	data            []Data
}

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println("Part one:", part_one(rows))
	fmt.Println("Part two:", part_two(rows))
}

func part_one(rows []string) uint64 {
	inputLine := strings.Split(rows[0], ": ")[1]
	inputSplit := strings.Split(inputLine, " ")
	inputs := []Input{}
	for i := 0; i < len(inputSplit); i++ {
		start := mustAtoInt64(inputSplit[i])
		inputs = append(inputs, Input{
			start: start,
			end:   start,
		})
	}

	rules := [][]Rule{}
	for i := 2; i < len(rows); i++ {
		if isTitle(rows[i]) {
			ruleset := []Rule{}
			for j := i + 1; j < len(rows); j++ {
				i++ // We increment the main loop for each row considered here
				if rows[j] == "" {
					break
				}
				data := parseDataRow(rows[j])
				rule := Rule{
					sourceStart: data.source,
					sourceEnd:   data.source + data.size - 1,
					delta:       data.destination - data.source,
				}
				ruleset = append(ruleset, rule)
			}
			rules = append(rules, ruleset)
		}
	}
	for _, rule := range rules {
		inputs = applyRules(inputs, rule)
	}

	minimumStartingPoint := uint64(0)
	for _, input := range inputs {
		if minimumStartingPoint == 0 || input.start < minimumStartingPoint {
			minimumStartingPoint = input.start
		}
	}
	return minimumStartingPoint

}
func part_two(rows []string) uint64 {
	inputLine := strings.Split(rows[0], ": ")[1]
	inputSplit := strings.Split(inputLine, " ")
	inputs := []Input{}
	for i := 0; i < len(inputSplit); i += 2 {
		start := mustAtoInt64(inputSplit[i])
		size := mustAtoInt64(inputSplit[i+1])
		inputs = append(inputs, Input{
			start: start,
			end:   start + size - 1,
		})
	}

	rules := [][]Rule{}
	for i := 2; i < len(rows); i++ {
		if isTitle(rows[i]) {
			ruleset := []Rule{}
			for j := i + 1; j < len(rows); j++ {
				i++ // We increment the main loop for each row considered here
				if rows[j] == "" {
					break
				}
				data := parseDataRow(rows[j])
				rule := Rule{
					sourceStart: data.source,
					sourceEnd:   data.source + data.size - 1,
					delta:       data.destination - data.source,
				}
				ruleset = append(ruleset, rule)
			}
			rules = append(rules, ruleset)
		}
	}

	for _, rule := range rules {
		inputs = applyRules(inputs, rule)
	}

	minimumStartingPoint := uint64(0)
	for _, input := range inputs {
		if minimumStartingPoint == 0 || input.start < minimumStartingPoint {
			minimumStartingPoint = input.start
		}
	}
	return minimumStartingPoint
}

func applyRules(inputs []Input, rules []Rule) []Input {
	checkSum := currentInputCount(inputs)
	processedInputs := []Input{}
	inputsToProcess := inputs

	for _, rule := range rules {
		nextRuleInputs := []Input{}
		for _, input := range inputsToProcess {

			// Check if rule does not apply
			if input.end < rule.sourceStart || input.start > rule.sourceEnd {
				nextRuleInputs = append(nextRuleInputs, input)
				continue
			}

			// Handle Input before the rule
			if input.start < rule.sourceStart {
				beforeInput := Input{
					start: input.start,
					end:   rule.sourceStart - 1,
				}
				input = Input{
					start: rule.sourceStart,
					end:   input.end,
				}
				nextRuleInputs = append(nextRuleInputs, beforeInput)
			}
			// Handle Input after the rule
			if input.end > rule.sourceEnd {
				afterInput := Input{
					start: rule.sourceEnd + 1,
					end:   input.end,
				}
				input = Input{
					start: input.start,
					end:   rule.sourceEnd,
				}
				nextRuleInputs = append(nextRuleInputs, afterInput)
			}
			// Handle Input inside rule
			if input.start >= rule.sourceStart && input.end <= rule.sourceEnd {
				newInput := Input{
					start: input.start + rule.delta,
					end:   input.end + rule.delta,
				}
				processedInputs = append(processedInputs, newInput)
			}
		}
		inputsToProcess = nextRuleInputs
	}
	// Handle inputs that have no applicable rules
	for _, input := range inputsToProcess {
		processedInputs = append(processedInputs, input)
	}

	newChecksum := currentInputCount(inputs)
	if checkSum != newChecksum {
		panic("AHH!")
	}
	return processedInputs
}

func currentInputCount(inputs []Input) uint64 {
	sum := uint64(0)
	for i := 0; i < len(inputs); i++ {
		sum += uint64(inputs[i].end) - uint64(inputs[i].start) + 1
	}
	return sum
}

func parseDataRow(str string) Data {
	split := strings.Split(str, " ")
	output := []uint64{}
	for _, val := range split {
		output = append(output, mustAtoInt64(val))
	}
	return Data{destination: output[0], source: output[1], size: output[2]}
}

func isTitle(str string) bool {
	return len(strings.Split(str, "-")) > 1
}

func mustAtoInt64(str string) uint64 {
	val, err := strconv.ParseUint(str, 10, 64)
	if err != nil {
		panic("mustAtoInt failure")
	}
	return val
}
