package main

import (
	"fmt"
	"strings"
	"testing"
)

func Test_input(t *testing.T) {
	fmt.Println("Start test_input")
	raw := `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
`
	rows := strings.Split(raw, "\n")
	result := part_two(rows)
	answer := uint64(46)
	if result != answer {
		t.Errorf("Failed test 2: %d != %d\n", result, answer)
	} else {
        fmt.Println("--- PASS")
    }
}

func Test_part_two(t *testing.T) {
	fmt.Println("~~~~ Start test_part_two")
	raw := `seeds: 1 1

seed-to-soil map:
2 1 1

soil-to-fertilizer map:
3 2 1
`
	fmt.Println(raw)
	rows := strings.Split(raw, "\n")
	result := part_two(rows)
	if result != 3 {
		t.Errorf("Failed test 1: %d != %d\n", result, 3)
	} else {
        fmt.Println("--- PASS")
    }
}

func Test_BaseCase(t *testing.T) {
	fmt.Println("~~~~ Start test_part_two")
	raw := `seeds: 1 1

seed-to-soil map:
2 1 1
`
	fmt.Println(raw)
	rows := strings.Split(raw, "\n")
	result := part_two(rows)
    answer := uint64(2)
	if result != answer {
		t.Errorf("Failed test 1: %d != %d\n", result, answer)
	} else {
        fmt.Println("--- PASS")
    }
}

func TestOverUnder(t *testing.T) {
	fmt.Println("~~~~ Start over under")
	raw := `seeds: 1 10

seed-to-soil map:
100 2 1

soil-to-fertilizer map:
200 3 1
`
    fmt.Println(raw)
	rows := strings.Split(raw, "\n")
	result := part_two(rows)
    answer := uint64(1)
	if result != answer {
		t.Errorf("test over_under %d != %d\n", result, answer)
	}
}
