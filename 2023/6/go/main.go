package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

const (
	// Path string = "./input.txt"
	Path string = "./full.txt"
)

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println("Part one:", part_one(rows))
	fmt.Println("Part two:", part_two(rows))
}

func part_one(rows []string) int {
	times := parseRow(rows[0])
	distances := parseRow(rows[1])
	product := 1

	if len(times) != len(distances) {
		panic("!!")
	}

	for i := 0; i < len(times); i++ {
		time := times[i]
		targetDistance := distances[i]

		// find the start win point
		start := 0
		end := time
		firstBreakpoint := bSearch(start, end, targetDistance, time)
		isStartOfTrueRange := calculateDistance(firstBreakpoint+1, time) > targetDistance
		var secondBreakpoint int
		if isStartOfTrueRange {
			for i := firstBreakpoint; i < end; i++ {
				distance := calculateDistance(i, time)
				if distance <= targetDistance {
					secondBreakpoint = i - 1
					break
				}
			}
		}
		winningGameCount := secondBreakpoint - firstBreakpoint + 1
		product *= winningGameCount
	}

	return product
}

func part_two(rows []string) int {
	time := parseRowPart2(rows[0])
	targetDistance := parseRowPart2(rows[1])
	product := 1

	// find the start win point
	start := 0
	end := time
	firstBreakpoint := bSearch(start, end, targetDistance, time)
	isStartOfTrueRange := calculateDistance(firstBreakpoint+1, time) > targetDistance

	var secondBreakpoint int
	if isStartOfTrueRange {
		for i := firstBreakpoint; i < end; i++ {
			distance := calculateDistance(i, time)
			if distance <= targetDistance {
				secondBreakpoint = i - 1
				break
			}
		}
	}
	winningGameCount := secondBreakpoint - firstBreakpoint + 1
	product *= winningGameCount
	return product
}

func bSearch(start int, end int, targetDistance int, time int) int {
	midpoint := calcMidpoint(start, end)
	distance := calculateDistance(midpoint, time)
	currentResult := distance > targetDistance
	if start == end {
		if currentResult {
			return midpoint
		}
		return midpoint + 1
	}
	if currentResult {
		return bSearch(start, midpoint, targetDistance, time)
	}
	return bSearch(midpoint+1, end, targetDistance, time)
}

// Non greedy
func calcMidpoint(start int, end int) int {
	size := end - start
	return end - int(math.Round(float64(size)/float64(2)))
}

func calculateDistance(holdTime int, runTime int) int {
	return holdTime * (runTime - holdTime)
}

func parseRow(x string) []int {
	sanitized := strings.Split(x, ":")[1]
	split := strings.Split(sanitized, " ")
	result := []int{}
	for _, val := range split {
		if len(val) > 0 {
			result = append(result, mustAtoI(val))
		}
	}
	return result
}
func parseRowPart2(x string) int {
	sanitized := strings.Split(x, ":")[1]
	split := strings.Split(sanitized, " ")
	result := ""
	for _, val := range split {
		if len(val) > 0 {
			result += val
		}
	}
	return mustAtoI(result)
}

func mustAtoI(x string) int {
	result, err := strconv.Atoi(x)
	if err != nil {
		panic("!!")
	}
	return result
}
