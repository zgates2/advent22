package main

import (
	"testing"
)

func Test_0_10(t *testing.T) {
	if calcMidpoint(0, 10) != 5 {
		t.Fatal("Failed 0 10")
	}
}
func Test_0_11(t *testing.T) {
	if calcMidpoint(0, 11) != 5 {
		t.Fatal("Failed 0 11")
	}
}
func Test_0_12(t *testing.T) {
	if calcMidpoint(0, 12) != 6 {
		t.Fatal("Failed 0 12")
	}
}
func Test_0_100(t *testing.T) {
	if calcMidpoint(0, 100) != 50 {
		t.Fatal("Failed 0 100")
	}
}
func Test_100_300(t *testing.T) {
	if calcMidpoint(100, 300) != 200 {
		t.Fatal("Failed 100 300")
	}
}

func Test_calc_dist(t *testing.T) {
	var distance int
	distance = calculateDistance(0, 7)
	if distance != 0 {
		t.Fatal("failed 0,7")
	}
	distance = calculateDistance(1, 7)
	if distance != 6 {
		t.Fatal("failed 1,7")
	}
    distance = calculateDistance(2, 7)
	if distance != 10 {
		t.Fatal("failed 2,7")
	}
    distance = calculateDistance(5, 7)
	if distance != 10 {
		t.Fatal("failed 5,7")
	}
    distance = calculateDistance(6, 7)
	if distance != 6 {
		t.Fatal("failed 6,7")
	}
    distance = calculateDistance(7, 7)
	if distance != 0 {
		t.Fatal("failed 7,7")
	}
}

