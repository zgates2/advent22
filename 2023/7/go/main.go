package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	// Path string = "./input.txt"
	Path string = "./full.txt"
)

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println("Part one:", Part_one(rows))
	fmt.Println("Part two:", Part_two(rows))
}

func Part_one(rows []string) uint64 {
	hands := Hands{}
	for _, row := range rows {
		hands = append(hands, parseRow(row))
	}
	fivers := Hands{}
	fours := Hands{}
	fullHouse := Hands{}
	threes := Hands{}
	twoPair := Hands{}
	pair := Hands{}
	highCard := Hands{}

	for _, hand := range hands {
		cardMap := map[rune]uint64{}
		for _, card := range hand.cards {
			cardRune := rune(card)
			if val, ok := cardMap[cardRune]; ok {
				cardMap[cardRune] = val + 1
			} else {
				cardMap[cardRune] = 1
			}
		}
		if isFiver(cardMap) {
			fivers = append(fivers, hand)
			continue
		}

		if isFour(cardMap) {
			fours = append(fours, hand)
			continue
		}

		if isFullHouse(cardMap) {
			fullHouse = append(fullHouse, hand)
			continue
		}
		if isThree(cardMap) {
			threes = append(threes, hand)
			continue
		}

		pairCount := pairCounter(cardMap)

		if pairCount == 2 {
			twoPair = append(twoPair, hand)
		} else if pairCount == 1 {
			pair = append(pair, hand)
		} else {
			highCard = append(highCard, hand)
		}
	}

	answer := uint64(0)
	rank := uint64(1)
	for _, val := range []Hands{highCard, pair, twoPair, threes, fullHouse, fours, fivers} {
		sort.Slice(val, func(i, j int) bool {
			return isHandBetter(val[i], val[j])
		})
		for i := len(val); i > 0; i-- {
			answer += rank * val[i-1].bid
			rank++
		}
	}
	return answer
}

func isHandBetter(i, j Hand) bool {
	a := i.cards
	b := j.cards
	for k := 0; k < CardCount; k++ {
		aScore := scoreACard(rune(a[k]))
		bScore := scoreACard(rune(b[k]))
		if aScore != bScore {
			return aScore > bScore
		}
	}
	panic("!!")
}

const (
	CardCount = 5
)

var CardValue = map[rune]uint64{
	'A': 14, 'K': 13, 'Q': 12, 'J': 11, 'T': 10, '9': 9, '8': 8, '7': 7,
	'6': 6, '5': 5, '4': 4, '3': 3, '2': 2,
}

func pairCounter(cardMap map[rune]uint64) uint64 {
	pairCount := uint64(0)
	for _, val := range cardMap {
		if val == 2 {
			pairCount++
		}
	}
	return pairCount
}

func isThree(cardMap map[rune]uint64) bool {
	if len(cardMap) > 3 {
		return false
	}
	for _, val := range cardMap {
		if val == 3 {
			return true
		}
	}
	return false
}

func isFiver(cardMap map[rune]uint64) bool {
	return len(cardMap) == 1
}

func isFour(cardMap map[rune]uint64) bool {
	if len(cardMap) > 2 {
		return false
	}
	for _, val := range cardMap {
		if val == 4 {
			return true
		}
	}
	return false
}

func isFullHouse(cardMap map[rune]uint64) bool {
	if len(cardMap) > 2 {
		return false
	}
	for _, val := range cardMap {
		if val != 3 && val != 2 {
			return false
		}
	}
	return true
}

func scoreACard(r rune) uint64 {
	return CardValue[r]
}

func parseRow(str string) Hand {
	split := strings.Split(str, " ")
	return Hand{cards: split[0], bid: mustAtoI(split[1])}
}

func mustAtoI(x string) uint64 {
	result, err := strconv.ParseUint(x, 10, 64)
	if err != nil {
		panic("!!")
	}
	return result
}

type Hand = struct {
	cards string
	bid   uint64
}
type Hands []Hand
