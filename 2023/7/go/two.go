package main

import (
	"sort"
)

func Part_two(rows []string) uint64 {
	hands := Hands{}
	for _, row := range rows {
		hands = append(hands, parseRow(row))
	}
	fivers := Hands{}
	fours := Hands{}
	fullHouse := Hands{}
	threes := Hands{}
	twoPair := Hands{}
	pair := Hands{}
	highCard := Hands{}

	for _, hand := range hands {
		cardMap := map[rune]uint64{}
		for _, card := range hand.cards {
			cardRune := rune(card)
			if val, ok := cardMap[cardRune]; ok {
				cardMap[cardRune] = val + 1
			} else {
				cardMap[cardRune] = 1
			}
		}
		if isFiver2(cardMap) {
			fivers = append(fivers, hand)
			continue
		}

		if isFour2(cardMap) {
			fours = append(fours, hand)
			continue
		}

		if isFullHouse2(cardMap) {
			fullHouse = append(fullHouse, hand)
			continue
		}
		if isThree2(cardMap) {
			threes = append(threes, hand)
			continue
		}

		pairCount := pairCounter(cardMap)
		jackCount := cardMap['J']

		if pairCount == 2 || jackCount == 2 {
			twoPair = append(twoPair, hand)
		} else if pairCount == 1 || jackCount == 1 {
			pair = append(pair, hand)
		} else {
			highCard = append(highCard, hand)
		}
	}

	answer := uint64(0)
	rank := uint64(1)
	for _, val := range []Hands{highCard, pair, twoPair, threes, fullHouse, fours, fivers} {
		sort.Slice(val, func(i, j int) bool {
			return isHandBetter2(val[i], val[j])
		})
		for i := len(val); i > 0; i-- {
			answer += rank * val[i-1].bid
			rank++
		}
	}
	return answer
}

var CardValue2 = map[rune]uint64{
	'A': 14, 'K': 13, 'Q': 12, 'T': 10, '9': 9, '8': 8, '7': 7,
	'6': 6, '5': 5, '4': 4, '3': 3, '2': 2, 'J': 1,
}

func isFiver2(cardMap map[rune]uint64) bool {
	jackFound := cardMap['J'] > 0
	if len(cardMap) == 1 {
		return true
	}
	if jackFound {
		return len(cardMap) == 2
	}
	return false
}

func isFour2(cardMap map[rune]uint64) bool {
	jackCount := cardMap['J']
	for key, val := range cardMap {
		if key == 'J' {
			if val == 4 {
				return true
			}
		} else if val+jackCount >= 4 {
			return true
		}
	}
	return false
}

func isFullHouse2(cardMap map[rune]uint64) bool {
	jackCount := cardMap['J']
	if jackCount == 3 {
		return true
	}
	pairCount := pairCounter(cardMap)
	if jackCount == 2 || jackCount == 1 {
		if pairCount == 2 {
			return true
		}
	}
	for _, val := range cardMap {
		if val != 3 && val != 2 {
			return false
		}
	}
	return true
}

func isThree2(cardMap map[rune]uint64) bool {
	jackCount := cardMap['J']
	for key, val := range cardMap {
		if key == 'J' {
			if jackCount == 3 {
				return true
			}
		} else if val+jackCount >= 3 {
			return true
		}
	}
	return false
}

func isHandBetter2(i, j Hand) bool {
	a := i.cards
	b := j.cards
	for k := 0; k < CardCount; k++ {
		aScore := scoreACard2(rune(a[k]))
		bScore := scoreACard2(rune(b[k]))
		if aScore != bScore {
			return aScore > bScore
		}
	}
	panic("!!")
}

func scoreACard2(r rune) uint64 {
	return CardValue2[r]
}
