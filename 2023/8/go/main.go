package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	//Path string = "./input.txt"
	// Path string = "./input2.txt"
	// Path string = "./input3.txt"
	Path string = "./full.txt"
)

type Direction = struct {
	left  string
	right string
}

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println(Part_one(rows))
	fmt.Println(Part_two(rows))
}

func Part_one(rows []string) int {
	choices := rows[0]
	directionsRaw := rows[2:]
	directions := parseDirections(directionsRaw)

	steps := 0
	location := "AAA"
	for true {
		choice := choices[steps%len(choices)]
		if choice == 'R' {
			location = directions[location].right
		} else {
			location = directions[location].left
		}
		steps++
		if checkIfEndpoint(location) {
			break
		}
	}
	return steps
}

func Part_two(rows []string) int {
	instructions := rows[0]
	directionsRaw := rows[2:]
	directions, locations := parseDirections2(directionsRaw)

	zIndices := []int{}
	for _, location := range locations {
		steps := 0
		zCount := 0
		for zCount < 1 {
			if checkIfEndpoint(location) {
				zCount++
				zIndices = append(zIndices, steps)
			}
			location = processInstruction(instructions[steps%len(instructions)], location, directions)
			steps++
		}
	}

	lcm := zIndices[0]
	for i := 1; i < len(zIndices); i++ {
		lcm = LCM(lcm, zIndices[i])
	}
	return lcm
}

func checkIfEndpoint(str string) bool {
	return 'Z' == str[2]
}

func processInstruction(r byte, location string, directions map[string]Direction) string {
	if r == 'R' {
		location = directions[location].right
	} else {
		location = directions[location].left
	}
	return location
}

func parseDirections(x []string) map[string]Direction {
	output := map[string]Direction{}
	for _, str := range x {
		split := strings.Split(str, " ")
		key := split[0]
		left := split[2][1:4]
		right := split[3][0:3]
		output[key] = Direction{left: left, right: right}
	}
	return output
}

func parseDirections2(x []string) (map[string]Direction, []string) {
	output := map[string]Direction{}
	startingPoints := []string{}
	for _, str := range x {
		split := strings.Split(str, " ")
		key := split[0]
		left := split[2][1:4]
		right := split[3][0:3]
		output[key] = Direction{left: left, right: right}
		if key[2] == 'A' {
			startingPoints = append(startingPoints, key)
		}
	}
	return output, startingPoints
}

func GCD(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

func LCM(a, b int) int {
	if a == 0 || b == 0 {
		return 0
	}
	g := GCD(a, b)
	return a / g * b
}
