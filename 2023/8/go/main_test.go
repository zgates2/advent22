package main

import (
	"testing"
)

func Test_checkEndpoint(t *testing.T) {
	if 'Z' != 'Z' {
		t.Fatal("Test case 1")
	}

	if "ZZZ"[2] != 'Z' {
		t.Fatal("Test case 2")
	}
}

func Test_GCD(t *testing.T) {
	if GCD(8, 12) != 4 {
		t.Fatal("Test case 1")
	}
	if GCD(10, 100) != 10 {
		t.Fatal("Test case 2")
	}
}

func Test_LCM(t *testing.T) {
	if LCM(7, 5) != 35 {
		t.Fatal("Test case 1")
	}
	if LCM(35, 3) != 105 {
		t.Fatal("Test case 2")
	}
}
