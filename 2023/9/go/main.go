package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	// Path string = "./input.txt"
	Path string = "./full.txt"
)

func main() {
	file, err := os.Open(Path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	rows := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		rows = append(rows, line)
	}
	fmt.Println(Part_one(rows))
	fmt.Println(Part_two(rows))
}

func Part_one(rows []string) int {
	values := [][]int{}
	for _, row := range rows {
		split := strings.Split(row, " ")
		rowValues := []int{}
		for _, str := range split {
			rowValues = append(rowValues, mustAtoI(str))
		}
		values = append(values, rowValues)
	}
	return solve(values)
}

func Part_two(rows []string) int {
	values := [][]int{}
	for _, row := range rows {

		split := strings.Split(row, " ")
		reverseSlice(split)
		rowValues := []int{}
		for _, str := range split {
			rowValues = append(rowValues, mustAtoI(str))
		}
		values = append(values, rowValues)
	}
	return solve(values)
}

func solve(values [][]int) int {
	sum := 0
	for _, line := range values {
		lineDifferences := [][]int{line}
		loop := true
		for loop {
			line, loop = processLine(line)
			lineDifferences = append(lineDifferences, line)
		}
		lastValues := []int{}
		for i := len(lineDifferences) - 1; i >= 0; i-- {
			if i == len(lineDifferences)-1 {
				lastValues = append(lastValues, 0)
			} else {
				childValue := lastValues[len(lastValues)-1]
				lineValue := lineDifferences[i][len(lineDifferences[i])-1]
				lastValues = append(lastValues, lineValue+childValue)
			}
		}
		sum += lastValues[len(lastValues)-1]
	}
	return sum
}

func reverseSlice(slice []string) {
	for i, j := 0, len(slice)-1; i < j; i, j = i+1, j-1 {
		slice[i], slice[j] = slice[j], slice[i]
	}
}

func processLine(line []int) ([]int, bool) {
	lineDifferences := []int{}
	foundNonZero := false
	for i := 0; i < len(line)-1; i += 1 {
		a := line[i]
		b := line[i+1]
		difference := b - a
		lineDifferences = append(lineDifferences, difference)
	}
	for _, val := range lineDifferences {
		if val != 0 {
			foundNonZero = true
			break
		}
	}
	return lineDifferences, foundNonZero
}

func mustAtoI(a string) int {
	val, err := strconv.Atoi(a)
	if err != nil {
		panic("not parsable")
	}
	return val
}
