package main

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fileContent, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	inputA := []int{}
	inputB := []int{}
	for _, line := range strings.Split(string(fileContent), "\n") {
		if line == "" {
			continue
		}
		inputs := strings.Split(line, "   ")
		if len(inputs) != 2 {
			log.Fatal(inputs)
		}
		a, err := strconv.Atoi(inputs[0])
		if err != nil {
			log.Fatal(err)
		}
		inputA = append(inputA, a)
		b, err := strconv.Atoi(inputs[1])
		if err != nil {
			log.Fatal(err)
		}
		inputB = append(inputB, b)
	}
	sort.Ints(inputA)
	sort.Ints(inputB)

	distance := 0
	for i := 0; i < len(inputA); i++ {
		diff := inputA[i] - inputB[i]
		if diff < 0 {
			diff = -diff
		}
		distance += diff
	}
	fmt.Println("Part 1:", distance)

	occurances := make(map[int]int)
	for _, val := range inputB {
		occurances[val] += 1
	}

	simularityScore := 0
	for _, val := range inputA {
		if occurances[val] > 0 {
			simularityScore += occurances[val] * val
		}
	}
	fmt.Println("Part 2:", simularityScore)
}
