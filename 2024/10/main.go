package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	input := string(file)
	grid := newGrid(input)

	startingPoints := grid.fetchStartingPoints()

	pathScoreTally := 0
	pathRating := 0
	for _, point := range startingPoints {
		coords := step(&grid, point)
		pathRating += len(coords)
		dedupMap := map[string]struct{}{}
		for _, c := range coords {
			dedupMap[c.print()] = struct{}{}
		}
		pathScoreTally += len(dedupMap)
	}
	fmt.Println("Score (pt1):", pathScoreTally)
	fmt.Println("Rating (pt2):", pathRating)
}

func step(g *grid, c coord) []coord {
	directions := []coord{
		{c.x, c.y - 1},
		{c.x, c.y + 1},
		{c.x - 1, c.y},
		{c.x + 1, c.y},
	}
	val, err := g.get(c)
	if err != nil {
		log.Fatal(err)
	}
	needle := val + 1
	needleIsEndpoint := needle == 9

	output := []coord{}
	for _, direction := range directions {
		val, err := g.get(direction)
		if err == nil {
			if val == needle {
				if needleIsEndpoint {
					output = append(output, direction)
				} else {
					output = append(output, step(g, direction)...)
				}
			}
		}
	}
	return output
}

func newGrid(input string) grid {
	values := [][]int{}
	for _, row := range strings.Split(input, "\n") {
		if row == "" {
			continue
		}
		rowValues := []int{}
		for _, val := range strings.Split(row, "") {
			num, err := strconv.Atoi(val)
			if err != nil {
				log.Fatal(err)
			}
			rowValues = append(rowValues, num)
		}
		values = append(values, rowValues)
	}
	return grid{values}
}

type coord struct {
	x int
	y int
}

type grid struct {
	values [][]int
}

func (c *coord) print() string {
	return fmt.Sprintf("%d,%d", c.x, c.y)
}

func (g *grid) fetchStartingPoints() []coord {
	output := []coord{}
	for y := range g.values {
		for x, val := range g.values[y] {
			if val == 0 {
				output = append(output, coord{x, y})
			}
		}
	}
	return output
}

func (g *grid) get(c coord) (int, error) {
	if c.y >= len(g.values) || c.y < 0 {
		return 0, errors.New("OOB")
	}
	if c.x >= len(g.values[0]) || c.x < 0 {
		return 0, errors.New("OOB")
	}
	return g.values[c.y][c.x], nil
}
