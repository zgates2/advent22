package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
)

func main() {
	input := "125 17"
	input = "5910927 0 1 47 261223 94788 545 7771"
	stones := make([]int, 0, 1024)
	for _, char := range strings.Split(input, " ") {
		value, err := strconv.Atoi(char)
		if err != nil {
			log.Fatal(err)
		}
		stones = append(stones, value)
	}
	stoneCount := 0
	maxRounds := 75
	cache := map[string]int{}
	for _, stone := range stones {
		result := calc(stone, maxRounds, cache)
		stoneCount += result
	}
	fmt.Println("Total", stoneCount)
}

func calc(stone, round int, cache map[string]int) int {
	if round == 0 {
		return 1
	}
	key := fmt.Sprintf("%d,%d", stone, round)
	if result, ok := cache[key]; ok {
		return result
	}
	round--
	result := 0
	if stone == 0 {
		result += calc(1, round, cache)
	} else {
		shouldSplit, a, b := calculateNumberOfDigits(stone)
		if shouldSplit {
			result += calc(a, round, cache)
			result += calc(b, round, cache)
		} else {
			result += calc(stone*2024, round, cache)
		}
	}
	cache[key] = result
	return result
}

func calculateNumberOfDigits(x int) (bool, int, int) {
	digits := 0
	temp := x
	for temp > 0 {
		temp /= 10
		digits++
	}
	if digits%2 == 1 {
		return false, 0, 0
	}

	divisor := int(math.Pow10(digits / 2))
	a, b := x/divisor, x%divisor
	return true, a, b
}
