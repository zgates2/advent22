package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type vector struct {
	x int
	y int
}

func (v1 *vector) add(v2 vector) {
	v1.x += v2.x
	v1.y += v2.y
}

func (v1 vector) minus(v2 vector) vector {
	return vector{
		x: v1.x - v2.x,
		y: v1.y - v2.y,
	}
}

func (v vector) mult(factor int) vector {
	return vector{
		x: v.x * factor,
		y: v.y * factor,
	}
}

type game struct {
	a vector
	b vector
	c vector
}

func main() {
	input := `
Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

Button A: X+26, Y+66
Button B: X+67, Y+21
Prize: X=12748, Y=12176

Button A: X+17, Y+86
Button B: X+84, Y+37
Prize: X=7870, Y=6450

Button A: X+69, Y+23
Button B: X+27, Y+71
Prize: X=18641, Y=10279

Button A: X+1, Y+1
Button B: X+2, Y2+
Prize: X=11, Y=11
`
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("AAAAAAHHHH", len(file))
	input = string(file)

	input = strings.TrimSpace(input)
	rounds := strings.Split(input, "\n\n")
	games := []game{}
	for _, round := range rounds {
		lines := strings.Split(round, "\n")
		a, b, c := lines[0], lines[1], lines[2]
		ax, err := strconv.Atoi(strings.Split(a, ",")[0][12:])
		if err != nil {
			log.Fatal(err)
		}
		ay, err := strconv.Atoi(strings.Split(a, "Y+")[1])
		if err != nil {
			log.Fatal(err)
		}
		bx, err := strconv.Atoi(strings.Split(b, ",")[0][12:])
		if err != nil {
			log.Fatal(err)
		}
		by, err := strconv.Atoi(strings.Split(b, "Y+")[1])
		if err != nil {
			log.Fatal(err)
		}
		cx, err := strconv.Atoi(strings.Split(c, ",")[0][9:])
		if err != nil {
			log.Fatal(err)
		}
		cy, err := strconv.Atoi(strings.Split(c, "Y=")[1])
		if err != nil {
			log.Fatal(err)
		}
		games = append(games, game{
			a: vector{
				x: ax, y: ay,
			},
			b: vector{
				x: bx, y: by,
			},
			c: vector{
				x: cx, y: cy,
			},
		})
	}
	fmt.Println("Part1:", playGame(games, 0), "\n-")
	fmt.Println("Part2:", playGame(games, 10_000_000_000_000))
}

func playGame(games []game, increment int) int {
	totalCost := 0
	for gamIdx, gam := range games {
		if increment > 0 {
			gam.c.x += increment
			gam.c.y += increment
		}
		fmt.Println("Game number:", gamIdx+1, gam)
		// Use GCD to rule out impossible games
		xGCD := gcd(gam.a.x, gam.b.x)
		if gam.c.x%xGCD != 0 {
			continue
		}
		yGCD := gcd(gam.a.y, gam.b.y)
		if gam.c.y%yGCD != 0 {
			continue
		}
		finalGCD := gcd(xGCD, yGCD)
		if (gam.c.x+gam.c.y)%finalGCD != 0 {
			continue
		}
		fmt.Println("Solvable!")
	}
	return totalCost
}

func lcm(a, b int) int {
	return (a * b) / gcd(a, b)
}

func gcd(a, b int) int {
	for b != 0 {
		a, b = b, a%b
	}
	return a
}

// 1545093008511 too low
// 141314180083648 too high
