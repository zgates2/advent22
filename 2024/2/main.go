package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Error reading file")
	}

	lines := strings.Split(string(file), "\n")
	approvedCount := 0
	retryApprovedCount := 0
	for _, line := range lines {
		if line == "" {
			continue
		}
		numbers := strings.Split(line, " ")
		result := processLine(numbers)
		if result {
			approvedCount++
		}
		retryResult := withRetry(numbers)
		if retryResult {
			retryApprovedCount++
		}
	}
	fmt.Println(approvedCount)
	fmt.Println(retryApprovedCount)
}

func processLine(numbers []string) bool {
	increasing := false
	decreasing := false
	for i := 0; i < len(numbers)-1; i++ {
		first, err := strconv.Atoi(numbers[i])
		if err != nil {
			log.Fatal("Error converting string to int")
		}
		second, err := strconv.Atoi(numbers[i+1])
		if err != nil {
			log.Fatal("Error converting string to int")
		}
		if first == second {
			return false
		}
		difference := first - second
		if difference == 0 {
			return false
		}
		if difference > 3 || difference < -3 {
			return false
		}
		if difference > 0 {
			decreasing = true
		}
		if difference < 0 {
			increasing = true
		}
		if increasing && decreasing {
			return false
		}
	}
	return true
}

func withRetry(numbers []string) bool {
	sliceAlloc := make([]string, len(numbers)-1)
	for i := range numbers {
		cloneSliceWithoutIdx(numbers, i, &sliceAlloc)
		result := processLine(sliceAlloc)
		if result {
			return true
		}
	}
	return false
}

func cloneSliceWithoutIdx(slice []string, idx int, sliceAlloc *[]string) {
	for i, j := 0, 0; i < len(slice); i++ {
		if i == idx {
			continue
		}
		(*sliceAlloc)[j] = slice[i]
		j++
	}
	return
}
