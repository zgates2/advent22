package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	inputBytes, err := os.ReadFile("input.txt")
	if err != nil {
		fmt.Println("Error reading file")
		return
	}
	input := string(inputBytes)
	prefix := "mul("

	tally := 0
	for i := 0; i < len(input); i++ {
		foundNeedle := false
		if input[i] == prefix[0] {
			if input[i:i+len(prefix)] == prefix {
				foundNeedle = true
			}
		}
		if !foundNeedle {
			continue
		}
		// Look ahead to find the closing bracket
		suffixIdx := -1
		for j := i + len(prefix); j < len(input); j++ {
			if input[j] == ')' {
				suffixIdx = j
				break
			}
		}
		if suffixIdx == -1 {
			continue
		}
		potentialInt := input[i+len(prefix) : suffixIdx]
		splitValues := strings.Split(potentialInt, ",")
		if len(splitValues) != 2 {
			continue
		}
		a, err := strconv.Atoi(splitValues[0])
		if err != nil {
			continue
		}
		b, err := strconv.Atoi(splitValues[1])
		if err != nil {
			continue
		}
		tally += a * b
	}

	// Part 2
	tally = 0
	doCmd := "do()"
	dontCmd := "don't()"
	active := true

	for i := 0; i < len(input); i++ {
		if !active && input[i] == doCmd[0] {
			if input[i:i+len(doCmd)] == doCmd {
				active = true
			}
		}
		if active && input[i] == dontCmd[0] {
			if input[i:i+len(dontCmd)] == dontCmd {
				active = false
			}
		}
		foundNeedle := false
		if active && input[i] == prefix[0] {
			if input[i:i+len(prefix)] == prefix {
				foundNeedle = true
			}
		}
		if !foundNeedle {
			continue
		}
		// Look ahead to find the closing bracket
		suffixIdx := -1
		for j := i + len(prefix); j < len(input); j++ {
			if input[j] == ')' {
				suffixIdx = j
				break
			}
		}
		if suffixIdx == -1 {
			continue
		}
		potentialInt := input[i+len(prefix) : suffixIdx]
		splitValues := strings.Split(potentialInt, ",")
		if len(splitValues) != 2 {
			continue
		}
		a, err := strconv.Atoi(splitValues[0])
		if err != nil {
			continue
		}
		b, err := strconv.Atoi(splitValues[1])
		if err != nil {
			continue
		}
		tally += a * b
	}
	fmt.Println("Part2 Tally:", tally)
}
