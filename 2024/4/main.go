package main

import (
	"fmt"
	"os"
	"strings"
)

const (
	part1Word = "XMAS"
	inputFile = "input.txt"
)

var directionVectors = [][]int{
	{1, 0},
	{-1, 0},
	{0, -1},
	{0, 1},
	{1, -1},
	{1, 1},
	{-1, -1},
	{-1, 1},
}

var crossWordVectors = [][]int{
	{-1, -1},
	{-1, 1},
	{1, -1},
	{1, 1},
}

func main() {
	file, err := os.ReadFile(inputFile)
	if err != nil {
		fmt.Println("Error reading file")
		os.Exit(1)
	}
	input := string(file)

	grid := [][]string{}
	for _, line := range strings.Split(input, "\n") {
		if len(line) == 0 {
			continue
		}
		grid = append(grid, strings.Split(line, ""))
	}

	part1Starts := make([][]int, 0, 32)
	part2Starts := make([][]int, 0, 32)

	for y := 0; y < len(grid); y++ {
		for x := 0; x < len(grid[0]); x++ {
			if grid[y][x] == string(part1Word[0]) {
				part1Starts = append(part1Starts, []int{x, y})
			}
			if grid[y][x] == "A" {
				part2Starts = append(part2Starts, []int{x, y})
			}
		}
	}

	wordFindCount := 0
	for _, start := range part1Starts {
		for _, vector := range directionVectors {
			if checkWord(start, vector[0], vector[1], part1Word, grid) {
				wordFindCount++
			}
		}
	}
	fmt.Printf("Part 1: %v\n", wordFindCount)

	part2Count := 0
	for _, start := range part2Starts {
		if checkCrossWord(start, grid, crossWordVectors) {
			part2Count++
		}
	}
	fmt.Printf("Part 2: %v\n", part2Count)
}

func checkWord(coord []int, xDelta, yDelta int, word string, grid [][]string) bool {
	for i, char := range word {
		x, y := coord[0]+xDelta*i, coord[1]+yDelta*i
		if !checkCoord(string(char), x, y, grid) {
			return false
		}
	}
	return true
}

func checkCrossWord(coord []int, grid [][]string, vectors [][]int) bool {
	foundCount := 0
	for _, vector := range vectors {
		x1, y1 := coord[0]+vector[0], coord[1]+vector[1]
		if checkCoord("M", x1, y1, grid) {
			x2, y2 := coord[0]-vector[0], coord[1]-vector[1]
			if checkCoord("S", x2, y2, grid) {
				if foundCount < 2 {
					foundCount++
				}
			}
		}
	}
	return foundCount == 2
}

func checkCoord(needle string, x, y int, grid [][]string) bool {
	if x < 0 || y < 0 || x >= len(grid[0]) || y >= len(grid) {
		return false
	}
	return grid[y][x] == needle
}
