package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal("Error reading file")
	}
	rawInput := strings.Split(string(file), "\n\n")

	pageLists := strings.Split(rawInput[1], "\n")

	rules := strings.Split(rawInput[0], "\n")
	ruleMap := make(map[string][]string)
	for _, rule := range rules {
		ruleParts := strings.Split(rule, "|")
		ruleMap[ruleParts[0]] = append(ruleMap[ruleParts[0]], ruleParts[1])
	}

	pageValuesWithIssues := [][]string{}
	tally := 0
	for _, pageList := range pageLists {
		if pageList == "" {
			continue
		}
		pageValues := strings.Split(pageList, ",")
		if len(pageValues)%2 == 0 {
			log.Fatal("Page list is not odd")
		}
		issueFound := false
	lineLoop:
		for i := len(pageValues) - 1; i > 0; i-- {
			// Scanning forwards
			pageValue := pageValues[i]
			subslice := pageValues[:i]
			if ruleMap[pageValue] == nil {
				continue
			}
			for _, prohibitedValue := range ruleMap[pageValue] {
				found, _ := contains(subslice, prohibitedValue)
				if found {
					pageValuesWithIssues = append(pageValuesWithIssues, pageValues)
					issueFound = true
					break lineLoop
				}
			}
		}
		if issueFound {
			continue
		}
		middleIndex := len(pageValues) / 2
		value, err := strconv.Atoi(pageValues[middleIndex])
		if err != nil {
			log.Fatal("Error converting to int", err)
		}
		tally += value
	}
	fmt.Println("Part one: ", tally)

	correctedLineTally := 0
	for _, pageValues := range pageValuesWithIssues {
		newPageValues, _ := processPages(pageValues, ruleMap, 0)

		middleValue := fetchMiddleValue(newPageValues)
		correctedLineTally += middleValue
	}
	fmt.Println("Part two: ", correctedLineTally)
}

// processPages recursively processes the pages and returns the corrected page
// values by reordering them base on the rules provided.
func processPages(pageValues []string, ruleMap map[string][]string, count int) ([]string, int) {
	if count > 100 {
		log.Fatal("Too many iterations")
	}
	for i := len(pageValues) - 1; i > 0; i-- {
		pageValue := pageValues[i]
		subslice := pageValues[:i]
		if ruleMap[pageValue] == nil {
			continue
		}
		for _, prohibitedValue := range ruleMap[pageValue] {
			found, _ := contains(subslice, prohibitedValue)
			if found {
				newPageValues := make([]string, 0, len(pageValues))
				for _, value := range pageValues {
					if value != prohibitedValue {
						newPageValues = append(newPageValues, value)
					}
					if value == pageValue {
						newPageValues = append(newPageValues, prohibitedValue)
					}
				}
				return processPages(newPageValues, ruleMap, count+1)
			}
		}
	}
	return pageValues, count
}

// fetchMiddleValue returns the middle value of a slice of strings
func fetchMiddleValue(pageValues []string) int {
	middleIndex := len(pageValues) / 2
	value, err := strconv.Atoi(pageValues[middleIndex])
	if err != nil {
		log.Fatal("Error converting to int", err)
	}
	return value
}

// contains checks if a string contains a value and returns the index
func contains(slice []string, needle string) (bool, int) {
	for idx, item := range slice {
		if item == needle {
			return true, idx
		}
	}
	return false, -1
}
