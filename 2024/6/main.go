package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	up      = "^"
	down    = "v"
	left    = "<"
	right   = ">"
	blocker = "#"
	OOB     = ""
)

func (g *grid) get(x, y int) string {
	if x < 0 || y < 0 || x >= len(g.values) || y >= len(g.values[0]) {
		return OOB
	}
	return g.values[y][x]
}

func (g *grid) updatePosition() {
	directions := []string{up, down, left, right}
	height := len(g.values)
	width := len(g.values[0])
outerLoop:
	for y := range height {
		for x := range width {
			if contains(directions, g.get(x, y)) {
				g.lastPosition = coord{x, y}
				break outerLoop
			}
		}
	}
	g.direction = g.get(g.lastPosition.x, g.lastPosition.y)
}

func (g *grid) tick() (bool, bool) {
	g.locationMap[toKey(g.lastPosition.x, g.lastPosition.y)] = true

	key := toKey(g.lastPosition.x, g.lastPosition.y) + "," + g.direction
	if g.positionHistory[key] {
		return false, true
	}
	g.positionHistory[key] = true

	var xVector, yVector int
	switch g.direction {
	case up:
		xVector, yVector = 0, -1
	case down:
		xVector, yVector = 0, 1
	case left:
		xVector, yVector = -1, 0
	case right:
		xVector, yVector = 1, 0
	}
	nextSectorValue := g.get(g.lastPosition.x+xVector, g.lastPosition.y+yVector)
	if nextSectorValue == OOB {
		g.isInBounds = false
		return false, false
	}
	if nextSectorValue == blocker {
		for g.get(g.lastPosition.x+xVector, g.lastPosition.y+yVector) == blocker {
			xVector, yVector, g.direction = rotateVectorClockwise(xVector, yVector, g.direction)
		}
	}
	g.lastPosition.x += xVector
	g.lastPosition.y += yVector
	g.values[g.lastPosition.y][g.lastPosition.x] = "O"
	return true, false
}

func (g *grid) print() {
	for _, line := range g.values {
		fmt.Println(line)
	}
	fmt.Println()
}

func newGrid(input string) grid {
	split := strings.Split(input, "\n")
	values := [][]string{}
	for _, line := range split {
		if line == "" {
			continue
		}
		values = append(values, strings.Split(line, ""))
	}

	g := grid{
		values: values, isInBounds: true,
		locationMap:     map[string]bool{},
		positionHistory: map[string]bool{},
	}
	g.updatePosition()
	return g
}

func main() {
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	input := string(file)
	values := [][]string{}
	for _, line := range strings.Split(input, "\n") {
		if line == "" {
			continue
		}
		values = append(values, strings.Split(line, ""))
	}

	g := newGrid(input)
	for {
		running, _ := g.tick()
		// g.print()
		if !running {
			break
		}
	}
	fmt.Println("Part one answer:", len(g.locationMap))

	loopCount := 0
	for coord := range g.locationMap {
		newGrid := newGrid(input)
		split := strings.Split(coord, ",")
		x, err := strconv.Atoi(split[0])
		if err != nil {
			log.Fatal(err)
		}
		y, err := strconv.Atoi(split[1])
		if err != nil {
			log.Fatal(err)
		}
		newGrid.values[y][x] = "#"
		var i int
		for i = 0; i < 1_000_000; i++ {
			running, loopDetected := newGrid.tick()
			if loopDetected {
				loopCount++
				break
			}
			if !running {
				break
			}
		}
		// newGrid.print()
	}
	fmt.Println("Loop count:", loopCount)
}

func contains(slice []string, needle string) bool {
	for _, s := range slice {
		if s == needle {
			return true
		}
	}
	return false
}

func rotateVectorClockwise(x, y int, direction string) (int, int, string) {
	outputDirection := ""
	xout, yout := 0, 0
	switch direction {
	case up:
		outputDirection = right
		xout, yout = 1, 0
	case right:
		outputDirection = down
		xout, yout = 0, 1
	case down:
		outputDirection = left
		xout, yout = -1, 0
	case left:
		outputDirection = up
		xout, yout = 0, -1
	}
	return xout, yout, outputDirection
}

func toKey(x, y int) string {
	return fmt.Sprintf("%d,%d", x, y)
}

type grid struct {
	values          [][]string
	lastPosition    coord
	direction       string
	isInBounds      bool
	locationMap     map[string]bool
	positionHistory map[string]bool
}

type coord struct {
	x, y int
}
