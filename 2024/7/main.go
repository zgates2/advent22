package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	split := strings.Split(string(file), "\n")
	partOneResult := 0
	partTwoResult := 0
	for _, line := range split {
		if line == "" {
			continue
		}
		lineSplit := strings.Split(line, ": ")
		testValue, err := strconv.Atoi(lineSplit[0])
		if err != nil {
			log.Fatal(err)
		}

		inputValuesSplit := strings.Split(lineSplit[1], " ")
		inputValues := []int{}
		for _, value := range inputValuesSplit {
			inputValue, err := strconv.Atoi(value)
			if err != nil {
				log.Fatal(err)
			}
			inputValues = append(inputValues, inputValue)
		}
		if line != fmt.Sprintf("%d: %s", testValue, strings.Join(inputValuesSplit, " ")) {
			log.Fatalf("Assertion FailureError: %s != %d: %s\n", line, testValue, strings.Join(inputValuesSplit, " "))
		}

		result, _ := process(testValue, inputValues[0], inputValues[1:], false)
		if result {
			partOneResult += testValue
		}

		result, _ = process(testValue, inputValues[0], inputValues[1:], true)
		if result {
			partTwoResult += testValue
		}
	}

	fmt.Printf("Part One: %d\n", partOneResult)
	fmt.Printf("Part Two: %d\n", partTwoResult)
}

func process(needle, previousValue int, nextValues []int, withConcats bool) (bool, []int) {
	// Base Case
	if len(nextValues) == 0 {
		if needle == previousValue {
			return true, nil
		}
		return false, nil
	}

	// Current Node's Values
	currentValue := nextValues[0]
	nextValues = nextValues[1:] // Reslice for next iteration

	add := previousValue + currentValue
	if add <= needle {
		foundNeedle, _ := process(needle, add, nextValues, withConcats)
		if foundNeedle {
			return true, nil
		}
	}

	// Recursive Case
	multiply := previousValue * currentValue
	if multiply <= needle {
		foundNeedle, _ := process(needle, multiply, nextValues, withConcats)
		if foundNeedle {
			return true, nil
		}
	}

	if withConcats {
		concat, err := strconv.Atoi(fmt.Sprintf("%d%d", previousValue, currentValue))
		if err != nil {
			log.Fatal(err)
		}
		if concat <= needle {
			foundNeedle, _ := process(needle, concat, nextValues, withConcats)
			if foundNeedle {
				return true, nil
			}
		}
	}

	// Final Leaf
	return false, nil
}
