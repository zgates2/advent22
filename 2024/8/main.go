package main

import (
	"fmt"
	"strings"
)

type Coord struct {
	x int
	y int
}

type Grid struct {
	values          [][]string
	radioStationMap map[string][]Coord
}

var ignoredValues = map[string]struct{}{
	"#": {},
	".": {},
}

func contains(value string) bool {
	_, exists := ignoredValues[value]
	return exists
}

func newGrid(rawInput string) Grid {
	values := [][]string{}
	radioStationMap := map[string][]Coord{}
	offset := 0
	for y, line := range strings.Split(rawInput, "\n") {
		y = y - offset
		if line == "" {
			offset += 1
			continue
		}
		rowValues := make([]string, len(line))
		for x, val := range strings.Split(line, "") {
			rowValues[x] = val
			if contains(val) {
				continue
			}
			if _, exists := radioStationMap[val]; !exists {
				radioStationMap[val] = []Coord{{x, y}}
			} else {
				radioStationMap[val] = append(radioStationMap[val], Coord{x, y})
			}
		}
		values = append(values, rowValues)
	}
	return Grid{
		values:          values,
		radioStationMap: radioStationMap,
	}
}

func (g *Grid) print() {
	for _, row := range g.values {
		fmt.Println(strings.Join(row, " "))
	}
}

func (g *Grid) validCoord(c Coord) bool {
	if c.y >= len(g.values) {
		return false
	}
	if c.y < 0 {
		return false
	}
	if c.x >= len(g.values[0]) {
		return false
	}
	if c.x < 0 {
		return false
	}
	return true
}

func (g *Grid) set(val string, c Coord) bool {
	if !g.validCoord(c) {
		return false
	}
	g.values[c.y][c.x] = val
	return true
}

func (g *Grid) countChar(needle string) int {
	count := 0
	for _, row := range g.values {
		for _, val := range row {
			if val == needle {
				count += 1
			}
		}
	}
	return count
}

func (g *Grid) countNotChar(needle string) int {
	count := 0
	for _, row := range g.values {
		for _, val := range row {
			if val != needle {
				count += 1
			}
		}
	}
	return count
}

func (c1 *Coord) equals(c2 Coord) bool {
	return c1.x == c2.x && c1.y == c2.y
}

func (c1 *Coord) distance(c2 Coord) Coord {
	run := c2.x - c1.x
	rise := c2.y - c1.y
	return Coord{run, rise}
}

func (c1 *Coord) sub(c2 Coord) Coord {
	return Coord{
		x: c1.x - c2.x,
		y: c1.y - c2.y,
	}
}

func main() {
	input := `
.........................p........................
......................h....C............M.........
..............................p....U..............
..5..................p............................
..6z...........................................C..
...............c...........zV.....................
...5.....c........................................
.Z.............h........S...z....9................
.O............................9...z........M..C...
..O....5..............................F..M..C.....
..Z.........5.c...............M....V..............
........ZO................q.......................
s...O................h..Uq.....7V...........4.....
.q.g..............c.............p.......4.........
............hZ.............................4G.....
6s...........................U.Q.....3............
.......6.................9.......Q.............3..
....s..D.........................6................
.............................................FL...
..................................................
..g...D.........q.....f.......Q...F....L......7...
...............2.........f.............V.L...4....
...................2.s...................f3......G
....g...........................v......7P.........
..2..g.............d.....v...........P.......1....
..............u.........f.............L........G..
.........l.D....u...............d........o..P.....
..................8...............9..1......o...7.
............l.....................................
...................l...S...........F.......o..U...
.......................u...S......................
..........l....u...............m...........P....G.
......................................1.8.......o.
..................................................
..................v.......S................0......
.............v........d.....1.....................
..................................................
..........D....................................0..
...................m.............H..........0.....
...................................d......0.......
..................................................
....Q.............................................
................................H.................
........................H....................8....
..................................................
..................................................
.........................................8........
.......................H3.........................
............................m.....................
................................m.................`

	grid := newGrid(input)
	// grid.print()
	for stationType, coords := range grid.radioStationMap {
		for _, primaryCoord := range coords {
			for _, secondaryCoord := range grid.radioStationMap[stationType] {
				if primaryCoord.equals(secondaryCoord) {
					continue
				}
				distance := primaryCoord.distance(secondaryCoord)
				target := primaryCoord.sub(distance)
				_ = grid.set("@", target)
			}
		}
	}
	// grid.print()
	fmt.Println(grid.countChar("@"))
	// part2
	for stationType, coords := range grid.radioStationMap {
		for _, primaryCoord := range coords {
			for _, secondaryCoord := range grid.radioStationMap[stationType] {
				if primaryCoord.equals(secondaryCoord) {
					continue
				}
				distance := primaryCoord.distance(secondaryCoord)
				target := primaryCoord.sub(distance)
				for grid.set("@", target) {
					target = target.sub(distance)
				}
			}
		}
	}
	// grid.print()
	fmt.Println(grid.countNotChar("."))
}
