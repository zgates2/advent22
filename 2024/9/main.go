package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Data struct {
	id            int
	size          int
	spaceAfter    int
	doNotUseSpace bool
	doNotMove     bool
}

func (d *Data) print() string {
	return fmt.Sprintf("Id: %d, Size: %d, Empties: %d", d.id, d.size, d.spaceAfter)
}

func main() {
	file, err := os.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}
	input := strings.TrimSpace(string(file))
	dataBlocks := loadDataBlocks(input)

	newBlocks := append([]Data{}, dataBlocks...)
	part1Total := part1(dataBlocks)
	newBlocks, err = part2(newBlocks)
	if err != nil {
		log.Fatal(err)
	}

	part2Total := 0
	counter := 0
	for _, block := range newBlocks {
		for range block.size {
			part2Total += counter * block.id
			counter++

		}
		for range block.spaceAfter {
			counter++
		}
	}
	fmt.Println("Part1:", part1Total)
	fmt.Println("Part2:", part2Total)
}

func loadDataBlocks(input string) []Data {
	if len(input)%2 != 0 {
		input += "0"
	}
	dataBlocks := []Data{}
	for i := 0; i < len(input)/2; i++ {
		dataIdx := i * 2
		spaceIdx := dataIdx + 1

		data, err := strconv.Atoi(string(
			input[dataIdx]),
		)
		if err != nil {
			log.Fatal(err)
		}

		var space int
		if spaceIdx >= len(input) {
			space = 0
		} else {
			space, err = strconv.Atoi(string(input[spaceIdx]))
			if err != nil {
				log.Fatal(err)
			}
		}
		block := Data{
			id:         i,
			size:       data,
			spaceAfter: space,
		}
		dataBlocks = append(dataBlocks, block)
	}
	return dataBlocks
}

func part1(dataBlocks []Data) int {
	tailBlockIdx := len(dataBlocks) - 1

	tally := 0
	filesProcessed := 0

	for idx, headBlock := range dataBlocks {
		for range headBlock.size {
			tally += filesProcessed * headBlock.id
			filesProcessed += 1
		}
		if tailBlockIdx == idx {
			break
		}
		for range headBlock.spaceAfter {
			for tailBlockIdx > 0 {
				tailBlock := &dataBlocks[tailBlockIdx]
				if tailBlock.size > 0 {
					tally += filesProcessed * tailBlock.id
					filesProcessed++
					tailBlock.size--
					break
				}
				tailBlockIdx--
			}
		}

	}
	return tally
}

func part2(dataBlocks []Data) ([]Data, error) {
	for tIdx := len(dataBlocks) - 1; tIdx >= 0; tIdx-- {
		tBlock := &dataBlocks[tIdx]
		if tBlock.doNotMove {
			continue
		}
		for hIdx := range dataBlocks {
			if hIdx >= tIdx {
				break
			}
			hBlock := &dataBlocks[hIdx]
			if hBlock.spaceAfter >= tBlock.size {

				prevBlock := &dataBlocks[tIdx-1]
				prevBlock.spaceAfter += tBlock.size + tBlock.spaceAfter

				tBlock.doNotMove = true
				tBlock.spaceAfter = hBlock.spaceAfter - tBlock.size

				hBlock.spaceAfter = 0

				newBlocks := []Data{}
				for i := range hIdx {
					newBlocks = append(newBlocks, dataBlocks[i])
				}
				newBlocks = append(newBlocks, *hBlock, *tBlock)
				for _, block := range dataBlocks[hIdx+1:] {
					if block.id == tBlock.id {
						continue
					}
					newBlocks = append(newBlocks, block)
				}

				return part2(newBlocks)
			}
		}
		tBlock.doNotMove = true
	}

	return dataBlocks, nil
}
